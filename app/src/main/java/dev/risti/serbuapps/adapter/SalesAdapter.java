package dev.risti.serbuapps.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.activities.NotificationListActivity;
import dev.risti.serbuapps.activities.SalesListActivity;
import dev.risti.serbuapps.interfaces.PaginationAdapterCallback;
import dev.risti.serbuapps.interfaces.RecyclerviewClick;
import dev.risti.serbuapps.model.Notification;
import dev.risti.serbuapps.model.Transaction;

import static dev.risti.serbuapps.utils.GlobalHelper.getTransactionId;

/**
 * Created by Suleiman on 19/10/16.
 */

public class SalesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<Transaction> list;
    private Context context;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;

    RecyclerviewClick listener;

    public SalesAdapter(Context context, RecyclerviewClick listener) {
        this.context = context;
        this.mCallback = (PaginationAdapterCallback) context;
        list = new ArrayList<>();
        this.listener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.list_item_notification, parent, false);

                viewHolder = new ViewHolder(viewItem);

                RecyclerView.ViewHolder finalViewHolder = viewHolder;
                viewItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(v, finalViewHolder.getAdapterPosition());

                    }
                });

                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Transaction result = list.get(position);

        switch (getItemViewType(position)) {

            case ITEM:
                final ViewHolder viewHolder = (ViewHolder) holder;

                //String id = "SA"+result.getId()+result.getCreated_at().replaceAll("[^\\d]", "");

                String id = "";

                if (result.getTransaction_id()!=null && result.getCreated_at() != null)
                    id = getTransactionId(result.getTransaction_id(), result.getCreated_at());
                else
                    id = context.getString(R.string.not_transaction_yet);

                viewHolder.tvTitle.setText(id);
                viewHolder.tvMessage.setText(result.getAddress());

                viewHolder.tvMessage.setTextIsSelectable(false);

                viewHolder.tvMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewHolder.cvRoot.performClick();
                    }
                });

                viewHolder.cvRoot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (result.getTransaction_id()!=null)
                            ((SalesListActivity) context).CustomDialog(result);
                        else
                            Toast.makeText(context, context.getString(R.string.not_transaction_yet), Toast.LENGTH_SHORT).show();

//                        Intent intent = new Intent(context, PoDetailActivity.class);
//                        intent.putExtra("data_po", result);
//                        context.startActivity(intent);
//                        ((ListExpiredActivity)context).finish();
                    }
                });
                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public void add(Transaction r) {
        list.add(r);
        notifyItemInserted(list.size() - 1);
    }

    public void addAll(List<Transaction> moveResults) {
        for (Transaction result : moveResults) {
            add(result);
        }
    }

    public void remove(Transaction r) {
        int position = list.indexOf(r);
        if (position > -1) {
            list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void clearAll() {
        isLoadingAdded = false;
        if (!list.isEmpty()){
            list.clear();
            notifyDataSetChanged();
        }

    }


    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Transaction());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = list.size() - 1;
        Transaction result = getItem(position);

        if (result != null) {
            list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Transaction getItem(int position) {

        if (list !=null){
            return list.get(position);
        }

        return null;

    }

    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param show
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(list.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    /**
     * Main list's content ViewHolder
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cvRoot;
        private TextView tvTitle;
        private TextView tvMessage;


        public ViewHolder(View itemView) {
            super(itemView);
            cvRoot = itemView.findViewById(R.id.cvRoot);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvMessage = itemView.findViewById(R.id.tvMessage);

            tvMessage.setPaintFlags(0);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}

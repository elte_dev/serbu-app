package dev.risti.serbuapps.adapter;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.activities.CategoryListActivity;
import dev.risti.serbuapps.activities.ProductListActivity;
import dev.risti.serbuapps.model.Category;
import dev.risti.serbuapps.utils.GlobalHelper;

import static dev.risti.serbuapps.utils.GlobalVars.KEY_CATEGORY_ID;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_USER_ROLE;

public class RecyclerViewDataAdapter extends RecyclerView.Adapter<RecyclerViewDataAdapter.ItemRowHolder> {

    private List<Category> dataList;
    private Context mContext;

    public RecyclerViewDataAdapter(Context context, List<Category> dataList) {
        this.dataList = dataList;
        this.mContext = context;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_section_category, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder itemRowHolder, int i) {

        final String sectionName = dataList.get(i).getCategory();
        final String id = dataList.get(i).getId();

        List singleSectionItems = dataList.get(i).getProduct();

        itemRowHolder.tvTitle.setText(sectionName);

        SectionListDataAdapter itemListDataAdapter = new SectionListDataAdapter(mContext, singleSectionItems);

        itemRowHolder.rv.setHasFixedSize(true);
        itemRowHolder.rv.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        itemRowHolder.rv.setAdapter(itemListDataAdapter);
        itemRowHolder.rv.setNestedScrollingEnabled(false);


       /*  itemRowHolder.recycler_view_list.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        //Allow ScrollView to intercept touch events once again.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                // Handle RecyclerView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });*/

        itemRowHolder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductListActivity.class);
                intent.putExtra(KEY_CATEGORY_ID, id);
                mContext.startActivity(intent);
                ((CategoryListActivity)mContext).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != dataList ? dataList.size() : 0);
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected RecyclerView rv;

        protected Button btnMore;



        public ItemRowHolder(View view) {
            super(view);

            this.tvTitle = view.findViewById(R.id.tvTitle);
            this.rv = view.findViewById(R.id.rv);
            this.btnMore= view.findViewById(R.id.btnMore);


        }

    }

}
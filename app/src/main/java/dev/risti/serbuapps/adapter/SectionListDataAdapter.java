package dev.risti.serbuapps.adapter;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.activities.ProductDetailActivity;
import dev.risti.serbuapps.activities.UpdateProductActivity;
import dev.risti.serbuapps.model.Product;
import dev.risti.serbuapps.utils.GlobalHelper;

import static dev.risti.serbuapps.utils.GlobalHelper.getPriceInIdr;
import static dev.risti.serbuapps.utils.GlobalHelper.getRole;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserId;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_ID;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private List<Product> itemsList;
    private Context mContext;

    public SectionListDataAdapter(Context context, List<Product> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_product, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        Product singleItem = itemsList.get(i);

        holder.tvTitle.setText(singleItem.getName());
        holder.tvPrice.setText(getPriceInIdr(singleItem.getPrice()));
        holder.ratingBar.setRating(Float.parseFloat(singleItem.getRate()));

        Glide.with(mContext)
                .load(singleItem.getImage())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .error(R.drawable.ic_preview)
                .into(holder.ivImage);

        holder.layoutContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.role.equals("seller") && holder.userId.equals(singleItem.getSeller_id())){
                    Intent intent = new Intent(mContext, UpdateProductActivity.class);
                    intent.putExtra(KEY_ID, singleItem.getId());
                    intent.putExtra("from", "section");
                    mContext.startActivity(intent);
                    ((Activity)mContext).finish();
                }else{
                    Intent intent = new Intent(mContext, ProductDetailActivity.class);
                    intent.putExtra(KEY_ID, singleItem.getId());
                    intent.putExtra("from", "section");
                    mContext.startActivity(intent);
                    ((Activity)mContext).finish();
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;
        protected TextView tvPrice;
        protected ImageView ivImage;
        protected RatingBar ratingBar;
        protected LinearLayout layoutContent;
        private String role;
        private String userId;


        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = view.findViewById(R.id.tvTitle);
            this.ivImage = view.findViewById(R.id.ivImage);
            this.tvPrice = view.findViewById(R.id.tvPrice);
            this.ratingBar = view.findViewById(R.id.ratingBar);
            this.layoutContent = view.findViewById(R.id.layoutContent);
            this.role = getRole();
            this.userId = getUserId();

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();
//
//                }
//            });


        }

    }

}
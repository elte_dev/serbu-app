package dev.risti.serbuapps.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.activities.CartListActivity;
import dev.risti.serbuapps.interfaces.OnJSONResponseCallback;
import dev.risti.serbuapps.interfaces.PaginationAdapterCallback;
import dev.risti.serbuapps.interfaces.RecyclerviewClick;
import dev.risti.serbuapps.model.TransactionDetail;

import static dev.risti.serbuapps.utils.GlobalHelper.getPriceInIdr;

/**
 * Created by Suleiman on 19/10/16.
 */

public class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // View Types
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<TransactionDetail> list;
    private Context context;

    private boolean isLoadingAdded = false;
    private boolean retryPageLoad = false;

    private PaginationAdapterCallback mCallback;

    private String errorMsg;

    private RecyclerviewClick listener;

    private TextView tvTotal;
    private int totalPayment = 0;
    private int totalSelected = 0;
    private int sellerId = -1;
    private int subTotal = -1;

    public CartAdapter(Context context, TextView tvTotal, RecyclerviewClick listener) {
        this.context = context;
        this.mCallback = (PaginationAdapterCallback) context;
        list = new ArrayList<>();
        this.listener = listener;
        this.tvTotal = tvTotal;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                View viewItem = inflater.inflate(R.layout.list_item_cart, parent, false);
                viewHolder = new ViewHolder(viewItem);

                RecyclerView.ViewHolder finalViewHolder = viewHolder;
                viewItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(v, finalViewHolder.getAdapterPosition());

                    }
                });

                break;
            case LOADING:
                View viewLoading = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(viewLoading);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TransactionDetail result = list.get(position);

        switch (getItemViewType(position)) {

            case ITEM:
                final ViewHolder viewHolder = (ViewHolder) holder;

                viewHolder.tvCounter.setText(result.getQty());
                viewHolder.tvProductName.setText(result.getProduct_name());
                viewHolder.tvOrderCount.setText(result.getQty());
                viewHolder.tvSellerName.setText(result.getSeller_name());
                viewHolder.tvPrice.setText(getPriceInIdr(result.getPrice()));

                viewHolder.tvSubTotal.setText(getPriceInIdr(String.valueOf(Integer.parseInt(viewHolder.tvCounter.getText().toString())*Integer.parseInt(result.getPrice()))));

                Glide.with(context)
                        .load(result.getProduct_image())
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .centerCrop()
                        .error(R.drawable.ic_preview)
                        .into(viewHolder.ivImage);



                viewHolder.ivDecrease.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (result.isSelected() == true){
                            Toast.makeText(context, context.getText(R.string.item_selected), Toast.LENGTH_SHORT).show();
                        }else{
                            int number = Integer.parseInt(viewHolder.tvCounter.getText().toString());
                            if (number>1){
                                number -= 1;

                                int finalNumber = number;

                                ((CartListActivity)context).upateOrderAmount(result.getId(), String.valueOf(number), new OnJSONResponseCallback() {
                                    @Override
                                    public void onJSONResponse(boolean success) {
                                        if (success){

                                            viewHolder.tvCounter.setText(String.valueOf(finalNumber));
                                            int subTotal = Integer.parseInt(viewHolder.tvCounter.getText().toString())*Integer.parseInt(result.getPrice());

                                            viewHolder.tvSubTotal.setText(getPriceInIdr(String.valueOf(subTotal)));
                                        } else{
                                            viewHolder.tvCounter.setText(String.valueOf(finalNumber+1));
                                            int subTotal = Integer.parseInt(viewHolder.tvCounter.getText().toString())*Integer.parseInt(result.getPrice());

                                            viewHolder.tvSubTotal.setText(getPriceInIdr(String.valueOf(subTotal)));
                                        }
                                    }
                                });
                            }else if (number==1){
                                number -= 1;
                                ((CartListActivity)context).showAlert();

                            }
                        }
                    }
                });

                viewHolder.ivIncrease.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (result.isSelected() == true){
                            Toast.makeText(context, context.getText(R.string.item_selected), Toast.LENGTH_SHORT).show();
                        }else{
                            int number = Integer.parseInt(viewHolder.tvCounter.getText().toString());
                            if (number<Integer.parseInt(result.getStock())){

                                number += 1;

                                int finalNumber = number;
                                ((CartListActivity)context).upateOrderAmount(result.getId(), String.valueOf(number), new OnJSONResponseCallback() {
                                    @Override
                                    public void onJSONResponse(boolean success) {
                                        if (success){
                                            viewHolder.tvCounter.setText(String.valueOf(finalNumber));
                                            int subTotal = Integer.parseInt(viewHolder.tvCounter.getText().toString())*Integer.parseInt(result.getPrice());

                                            viewHolder.tvSubTotal.setText(getPriceInIdr(String.valueOf(subTotal)));
                                        } else{
                                            viewHolder.tvCounter.setText(String.valueOf(finalNumber-1));
                                            int subTotal = Integer.parseInt(viewHolder.tvCounter.getText().toString())*Integer.parseInt(result.getPrice());

                                            viewHolder.tvSubTotal.setText(getPriceInIdr(String.valueOf(subTotal)));
                                        }
                                    }
                                });

                            }else{
                                Toast.makeText(context, context.getString(R.string.max_stock, result.getProduct_name(), result.getStock()), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                viewHolder.view.setBackgroundColor(result.isSelected() ?
                        ContextCompat.getColor(context, R.color.md_deep_orange_300) :
                        ContextCompat.getColor(context, R.color.md_white_1000));

                if (result.isSelected()){
                    totalPayment += (Integer.parseInt(viewHolder.tvCounter.getText().toString())*Integer.parseInt(result.getPrice()));
                    totalSelected += 1;
                }

                tvTotal.setText(String.valueOf(totalPayment));

                viewHolder.ivImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (sellerId!=-1 && sellerId==Integer.parseInt(result.getSeller_id()) || sellerId==-1){
                            result.setSelected(!result.isSelected());
                            viewHolder.view.setBackgroundColor(result.isSelected() ?
                                    ContextCompat.getColor(context, R.color.md_deep_orange_300) :
                                    ContextCompat.getColor(context, R.color.md_white_1000));

                            if (result.isSelected()){
                                totalPayment += (Integer.parseInt(viewHolder.tvCounter.getText().toString())*Integer.parseInt(result.getPrice()));
                                totalSelected += 1;
                                sellerId = Integer.parseInt(result.getSeller_id());
                            }else{
                                totalPayment -= (Integer.parseInt(viewHolder.tvCounter.getText().toString())*Integer.parseInt(result.getPrice()));
                                totalSelected -= 1;
                            }

                            tvTotal.setText(String.valueOf(totalPayment));
                            if (totalSelected==0){
                                sellerId = -1;
                                tvTotal.setText("0");
                            }

                        }else{
                            Toast.makeText(context, context.getText(R.string.seller_does_not_same), Toast.LENGTH_SHORT).show();
                        }
                    }


                });

                viewHolder.ivRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                break;

            case LOADING:
                LoadingVH loadingVH = (LoadingVH) holder;

                if (retryPageLoad) {
                    loadingVH.mErrorLayout.setVisibility(View.VISIBLE);
                    loadingVH.mProgressBar.setVisibility(View.GONE);

                    loadingVH.mErrorTxt.setText(
                            errorMsg != null ?
                                    errorMsg :
                                    context.getString(R.string.error));

                } else {
                    loadingVH.mErrorLayout.setVisibility(View.GONE);
                    loadingVH.mProgressBar.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public void add(TransactionDetail r) {
        list.add(r);
        notifyItemInserted(list.size() - 1);
    }

    public void addAll(List<TransactionDetail> moveResults) {
        for (TransactionDetail result : moveResults) {
            add(result);
        }
    }

    public void remove(TransactionDetail r) {
        int position = list.indexOf(r);
        if (position > -1) {
            list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public void clearAll() {
        isLoadingAdded = false;
        if (!list.isEmpty()){
            list.clear();
            notifyDataSetChanged();
        }

    }


    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new TransactionDetail());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = list.size() - 1;
        TransactionDetail result = getItem(position);

        if (result != null) {
            list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public TransactionDetail getItem(int position) {

        if (list !=null){
            return list.get(position);
        }

        return null;

    }

    /**
     * Displays Pagination retry footer view along with appropriate errorMsg
     *
     * @param show
     * @param errorMsg to display if page load fails
     */
    public void showRetry(boolean show, @Nullable String errorMsg) {
        retryPageLoad = show;
        notifyItemChanged(list.size() - 1);

        if (errorMsg != null) this.errorMsg = errorMsg;
    }

    /**
     * Main list's content ViewHolder
     */
    protected class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cvRoot;
        private ImageView ivRemove;
        private ImageView ivIncrease;
        private ImageView ivDecrease;
        private ImageView ivImage;
        private TextView tvCounter;
        private TextView tvProductName;
        private TextView tvOrderCount;
        private TextView tvSellerName;
        private TextView tvPrice;
        private TextView tvSubTotal;
        private View view;


        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            cvRoot = itemView.findViewById(R.id.cvRoot);
            ivRemove = itemView.findViewById(R.id.ivRemove);
            ivIncrease = itemView.findViewById(R.id.ivIncrease);
            ivDecrease = itemView.findViewById(R.id.ivDecrease);
            ivImage = itemView.findViewById(R.id.ivImage);
            tvCounter = itemView.findViewById(R.id.tvCounter);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            tvOrderCount = itemView.findViewById(R.id.tvOrderCount);
            tvSellerName = itemView.findViewById(R.id.tvSellerName);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvSubTotal = itemView.findViewById(R.id.tvSubTotal);

        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ProgressBar mProgressBar;
        private ImageButton mRetryBtn;
        private TextView mErrorTxt;
        private LinearLayout mErrorLayout;

        public LoadingVH(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.loadmore_progress);
            mRetryBtn = itemView.findViewById(R.id.loadmore_retry);
            mErrorTxt = itemView.findViewById(R.id.loadmore_errortxt);
            mErrorLayout = itemView.findViewById(R.id.loadmore_errorlayout);

            mRetryBtn.setOnClickListener(this);
            mErrorLayout.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.loadmore_retry:
                case R.id.loadmore_errorlayout:

                    showRetry(false, null);
                    mCallback.retryPageLoad();

                    break;
            }
        }
    }

}

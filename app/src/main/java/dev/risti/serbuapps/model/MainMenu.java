package dev.risti.serbuapps.model;

public class MainMenu {
    private String menu;

    public MainMenu(String menu) {
        this.menu = menu;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }
}

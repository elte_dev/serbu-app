package dev.risti.serbuapps.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pratap.kesaboyina on 30-11-2015.
 */
public class Category {

    private String id;
    private String category;
    private List<Product> product;

    public Category() {

    }
    public Category(String category, List<Product> product) {
        this.category = category;
        this.product = product;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Product> getProduct() {
        return product;
    }

    public void setProduct(List<Product> product) {
        this.product = product;
    }


}

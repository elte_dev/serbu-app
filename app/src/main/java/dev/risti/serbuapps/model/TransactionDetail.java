package dev.risti.serbuapps.model;

import android.os.Parcel;
import android.os.Parcelable;

public class TransactionDetail implements Parcelable {

    private String message;
    private String id;
    private String transaction_id;
    private String seller_id;
    private String seller_name;
    private String buyer_id;
    private String buyer_name;
    private String product_id;
    private String product_name;
    private String product_image;
    private String category_id;
    private String qty;
    private String price;
    private String note;
    private String transaction_note;
    private String proof_of_payment;
    private String stock;
    private String rate;
    private String status;
    private boolean isSelected = false;

    public static final Parcelable.Creator<TransactionDetail> CREATOR = new Parcelable.Creator<TransactionDetail>() {
        public TransactionDetail createFromParcel(Parcel in) {
            return new TransactionDetail(in);
        }

        public TransactionDetail[] newArray(int size) {
            return new TransactionDetail[size];
        }
    };

    protected TransactionDetail(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.transaction_id = ((String) in.readValue((String.class.getClassLoader())));
        this.seller_id = ((String) in.readValue((String.class.getClassLoader())));
        this.seller_name = ((String) in.readValue((String.class.getClassLoader())));
        this.buyer_id = ((String) in.readValue((String.class.getClassLoader())));
        this.buyer_name = ((String) in.readValue((String.class.getClassLoader())));
        this.product_id = ((String) in.readValue((String.class.getClassLoader())));
        this.product_name = ((String) in.readValue((String.class.getClassLoader())));
        this.product_image = ((String) in.readValue((String.class.getClassLoader())));
        this.category_id = ((String) in.readValue((String.class.getClassLoader())));
        this.qty = ((String) in.readValue((String.class.getClassLoader())));
        this.price = ((String) in.readValue((String.class.getClassLoader())));
        this.note = ((String) in.readValue((String.class.getClassLoader())));
        this.transaction_note = ((String) in.readValue((String.class.getClassLoader())));
        this.proof_of_payment = ((String) in.readValue((String.class.getClassLoader())));
        this.rate = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
    }

    public TransactionDetail() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getBuyer_id() {
        return buyer_id;
    }

    public void setBuyer_id(String buyer_id) {
        this.buyer_id = buyer_id;
    }

    public String getBuyer_name() {
        return buyer_name;
    }

    public void setBuyer_name(String buyer_name) {
        this.buyer_name = buyer_name;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTransaction_note() {
        return transaction_note;
    }

    public void setTransaction_note(String transaction_note) {
        this.transaction_note = transaction_note;
    }

    public String getProof_of_payment() {
        return proof_of_payment;
    }

    public void setProof_of_payment(String proof_of_payment) {
        this.proof_of_payment = proof_of_payment;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(transaction_id);
        dest.writeValue(seller_id);
        dest.writeValue(seller_name);
        dest.writeValue(buyer_id);
        dest.writeValue(buyer_name);
        dest.writeValue(product_id);
        dest.writeValue(product_name);
        dest.writeValue(product_image);
        dest.writeValue(category_id);
        dest.writeValue(qty);
        dest.writeValue(price);
        dest.writeValue(note);
        dest.writeValue(transaction_note);
        dest.writeValue(proof_of_payment);
        dest.writeValue(rate);
        dest.writeValue(status);
    }

    public int describeContents() {
        return this.hashCode();
    }

}
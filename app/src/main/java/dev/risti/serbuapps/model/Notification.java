package dev.risti.serbuapps.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Notification implements Parcelable {

    private String id;
    private String transaction_id;
    private String message;
    private String sender_id;
    private String sender_name;
    private String recepient_id;
    private String status;
    private String created_at;

    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator<Notification>() {
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    protected Notification(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.transaction_id = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.sender_id = ((String) in.readValue((String.class.getClassLoader())));
        this.sender_name = ((String) in.readValue((String.class.getClassLoader())));
        this.recepient_id = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.created_at = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Notification() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getRecepient_id() {
        return recepient_id;
    }

    public void setRecepient_id(String recepient_id) {
        this.recepient_id = recepient_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(transaction_id);
        dest.writeValue(message);
        dest.writeValue(sender_id);
        dest.writeValue(sender_name);
        dest.writeValue(recepient_id);
        dest.writeValue(status);
        dest.writeValue(created_at);
    }

    public int describeContents() {
        return this.hashCode();
    }

}

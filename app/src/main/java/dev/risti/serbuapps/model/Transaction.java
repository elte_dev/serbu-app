package dev.risti.serbuapps.model;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Transaction implements Parcelable
{

    private String message;
    private String transaction_id;
    private String payment_method;
    private String transaction_note;
    private String proof_of_payment;
    private String address;
    private String created_at;
    private List<TransactionDetail> transactionDetail = null;


    public static final Parcelable.Creator<Transaction> CREATOR = new Parcelable.Creator<Transaction>() {
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    protected Transaction(Parcel in) {
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.transaction_id = ((String) in.readValue((String.class.getClassLoader())));
        this.payment_method = ((String) in.readValue((String.class.getClassLoader())));
        this.transaction_note = ((String) in.readValue((String.class.getClassLoader())));
        this.proof_of_payment = ((String) in.readValue((String.class.getClassLoader())));
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.created_at = ((String) in.readValue((String.class.getClassLoader())));
        if (transactionDetail!=null)
            in.readList(this.transactionDetail, (TransactionDetail.class.getClassLoader()));
    }

    public Transaction() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getTransaction_note() {
        return transaction_note;
    }

    public void setTransaction_note(String transaction_note) {
        this.transaction_note = transaction_note;
    }

    public String getProof_of_payment() {
        return proof_of_payment;
    }

    public void setProof_of_payment(String proof_of_payment) {
        this.proof_of_payment = proof_of_payment;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public List<TransactionDetail> getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(List<TransactionDetail> transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(message);
        dest.writeValue(transaction_id);
        dest.writeValue(payment_method);
        dest.writeValue(transaction_note);
        dest.writeValue(proof_of_payment);
        dest.writeValue(address);
        dest.writeValue(created_at);
        if (transactionDetail!=null)
            dest.writeList(transactionDetail);
    }

    public int describeContents() {
        return this.hashCode();
    }

}
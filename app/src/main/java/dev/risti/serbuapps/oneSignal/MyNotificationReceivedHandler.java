package dev.risti.serbuapps.oneSignal;

import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import dev.risti.serbuapps.MyApp;

/**
 * Created by ELTE on 11/7/2017.
 */

public class MyNotificationReceivedHandler  implements OneSignal.NotificationReceivedHandler {
    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;
        String customKey;

        if (data != null) {
            //While sending a Push notification from OneSignal dashboard
            // you can send an addtional data named "customkey" and retrieve the value of it and do necessary operation
            customKey = data.optString("customkey", null);
            if (customKey != null){


                //I do sth here
                Log.i("OneSignalExample", "customkey set with value: " + customKey);
                NotificationManager nMgr = (NotificationManager) MyApp.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
                nMgr.cancelAll();
            }

        }
    }
}
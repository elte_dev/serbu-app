package dev.risti.serbuapps;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.support.multidex.MultiDex;

import com.androidnetworking.AndroidNetworking;
import com.onesignal.OneSignal;

import dev.risti.serbuapps.oneSignal.MyNotificationOpenedHandler;
import dev.risti.serbuapps.oneSignal.MyNotificationReceivedHandler;


/**
 * Created by ELTE on 11/2/2017.
 */

public class MyApp extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        MyApp.context = getApplicationContext();
        AndroidNetworking.initialize(getApplicationContext());

        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler( new MyNotificationReceivedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static Context getContext(){
        return MyApp.context;
    }

}

package dev.risti.serbuapps.utils;

import android.os.Environment;

import net.rehacktive.waspdb.WaspDb;
import net.rehacktive.waspdb.WaspFactory;
import net.rehacktive.waspdb.WaspHash;

import java.io.File;

import static dev.risti.serbuapps.utils.GlobalHelper.SELECT_USER;

public class GlobalVars {
    public static final File BASE_DIR = Environment.getExternalStorageDirectory();
    public static final String EXTERNAL_DIR_FILES = "/serbu_apps";
    public static final String PICTURES_DIR_FILES = "/Pictures";
    public static final String IMAGES_DIR = BASE_DIR + EXTERNAL_DIR_FILES + "/images/";
    public static final String BASE_IP = "http://myskripsi.xyz/api_serbu/";


    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_CATEGORY_ID = "category_id";
    public static final String KEY_USER_ROLE = "user_role";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_ID = "id";
    public static final String KEY_OWN_PRODUCT = "own_product";
    public static final String KEY_ACTION = "action";
    public static final String KEY_ACTION_EDIT = "action_edit";
    public static final String KEY_ACTION_INSERT = "action_insert";
    public static final String KEY_TRUE = "TRUE";
    public static final String KEY_FALSE = "FALSE";
    public static final String KEY_CART_COUNTER = "cartCounter";
    public static int GPS_INTERVAL = 10*1000;

    public static final String DB_MASTER = "DB_SERBU_APPS";
    public static final String DB_PASS = "SERBU12344321SERBU";
    public static final WaspDb db = WaspFactory.openOrCreateDatabase(getDatabasePath(), DB_MASTER, getDatabasePass());
    public static final WaspHash userHash = db.openOrCreateHash(SELECT_USER);

    public static String getDatabasePath(){
        File file = new File(Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/db/" + Encrypts.encrypt("POST"));
        if(!file.exists())file.mkdirs();
        return Environment.getExternalStorageDirectory() + EXTERNAL_DIR_FILES +"/db/" + Encrypts.encrypt("POST");
    }

    public static String getDatabasePass(){
        return Encrypts.encrypt(DB_PASS);
    }
    public static WaspHash getTableHash(String tableName){
        WaspDb db = WaspFactory.openOrCreateDatabase(GlobalVars.getDatabasePath(),
                GlobalVars.DB_MASTER,
                GlobalVars.getDatabasePass());
        WaspHash returnedTable = db.openOrCreateHash(tableName);
        return returnedTable;
    }
}

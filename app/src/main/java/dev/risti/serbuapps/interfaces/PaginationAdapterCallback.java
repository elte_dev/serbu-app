package dev.risti.serbuapps.interfaces;

public interface PaginationAdapterCallback {

    void retryPageLoad();
}

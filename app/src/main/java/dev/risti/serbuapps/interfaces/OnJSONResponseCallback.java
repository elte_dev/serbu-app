package dev.risti.serbuapps.interfaces;

public interface OnJSONResponseCallback {
    public void onJSONResponse(boolean success);
}

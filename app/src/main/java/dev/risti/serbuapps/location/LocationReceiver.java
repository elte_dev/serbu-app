package dev.risti.serbuapps.location;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;

import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import static dev.risti.serbuapps.utils.GlobalHelper.getUserEmail;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserId;

/**
 * @author josevieira
 */
public class LocationReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (null != intent && intent.getAction().equals("dev.elte.ActivityRecognition.RestartSensor") ) {
            Location locationData = (Location) intent.getParcelableExtra(SettingsLocationTracker.LOCATION_MESSAGE);

            //currentLocation = locationData.getLatitude()+", "+locationData.getLongitude();

            //Toast.makeText(context, "Posisi :" + locationData.getLatitude() + ", " + locationData.getLongitude(), Toast.LENGTH_SHORT).show();
            sendOneSignalData();
        }
    }

    private void sendOneSignalData(){

        JSONObject tags = new JSONObject();
        try {
            tags.put("userId", getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        OneSignal.sendTags(tags);
        OneSignal.promptLocation();
        OneSignal.setEmail(getUserEmail());
    }
}
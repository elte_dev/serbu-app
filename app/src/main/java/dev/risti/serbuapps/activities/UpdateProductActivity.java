package dev.risti.serbuapps.activities;

import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.travijuu.numberpicker.library.NumberPicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.adapter.SpinnerAdapter;
import dev.risti.serbuapps.imagepicker.RxImageConverters;
import dev.risti.serbuapps.imagepicker.RxImagePicker;
import dev.risti.serbuapps.imagepicker.Sources;
import dev.risti.serbuapps.model.Category;
import dev.risti.serbuapps.model.Product;
import dev.risti.serbuapps.model.User;
import io.reactivex.Observable;

import static dev.risti.serbuapps.utils.GlobalHelper.compressFoto;
import static dev.risti.serbuapps.utils.GlobalHelper.convertFileToContentUri;
import static dev.risti.serbuapps.utils.GlobalHelper.deleteRecursive;
import static dev.risti.serbuapps.utils.GlobalHelper.encodeFileBase64;
import static dev.risti.serbuapps.utils.GlobalHelper.getFileNameFromURL;
import static dev.risti.serbuapps.utils.GlobalHelper.getMimeTypeFromUri;
import static dev.risti.serbuapps.utils.GlobalHelper.getPath;
import static dev.risti.serbuapps.utils.GlobalHelper.getPriceInIdr;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserId;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_DIR;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.EXTERNAL_DIR_FILES;
import static dev.risti.serbuapps.utils.GlobalVars.IMAGES_DIR;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_ID;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_OWN_PRODUCT;

public class UpdateProductActivity extends AppCompatActivity {
    private NumberPicker numberPicker;

    private Gson gson;

    private ImageView ivCamera;
    private ImageView ivGalery;
    private ImageView ivImage;

    //open camera
    private RadioGroup converterRadioGroup;
    private Uri photoUri = null;
    private Uri finalPhotoUri = null;
    private File compressedImage = null;
    private File tempFile= null;
    private String photoExt = "";
    private String encodePhoto = "";
    private Bitmap theBitmap = null;

    private String id;

    private Spinner spin;
    SpinnerAdapter spinAdapter;

    private String catId;
    private String price = "0";
    private double finalPrice;

    private EditText txtProductName;
    private EditText txtDesc;
    private EditText txtPrice;
    private TextView tvCancel;
    private Button btnUpdate;

    private float rate;
    private int stock;
    private String sellerId;
    private String productId;
    private String categoryId = "";
    private LinearLayout seller;
    private String tempId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_product);

        gson = new Gson();

        ivImage = findViewById(R.id.ivImage);
        ivCamera = findViewById(R.id.ivCamera);
        ivGalery = findViewById(R.id.ivGalery);
        spin = findViewById(R.id.spin);
        txtProductName = findViewById(R.id.txtProductName);
        txtDesc = findViewById(R.id.txtDesc);
        txtPrice = findViewById(R.id.txtPrice);
        tvCancel = findViewById(R.id.tvCancel);
        btnUpdate = findViewById(R.id.btnUpdate);

        numberPicker = findViewById(R.id.numberPicker);
        numberPicker.setDisplayFocusable(true);
        numberPicker.setMax(10000);
        numberPicker.setMin(0);
        numberPicker.setUnit(1);
        numberPicker.setValue(0);

        if (getIntent().hasExtra(KEY_ID)) {
            tempId = getIntent().getStringExtra(KEY_ID);
            getData(getIntent().getStringExtra(KEY_ID));
        }

        converterRadioGroup = findViewById(R.id.radio_group);
        converterRadioGroup.check(R.id.radio_file);

        if (RxImagePicker.with(UpdateProductActivity.this).getActiveSubscription() != null) {
            RxImagePicker.with(UpdateProductActivity.this).getActiveSubscription().subscribe(this::onImagePicked);
        }

        id = String.valueOf(System.currentTimeMillis());
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromSource(Sources.CAMERA);
            }
        });

        ivGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromSource(Sources.GALLERY);
            }
        });



        txtPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtPrice.removeTextChangedListener(this);
                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");

                        finalPrice = Double.parseDouble(originalString);
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    txtPrice.setText(formattedString);
                    txtPrice.setSelection(txtPrice.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                txtPrice.addTextChangedListener(this);
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tempId!=null && !tempId.equals(""))
                    update();
                else
                    insertNew();
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog();
                //resetData();
                //intentToMain();
            }
        });

        getCategory();

    }

    private void pickImageFromSource(Sources source) {

        RxImagePicker.with(UpdateProductActivity.this).requestImage(source)
                .flatMap(uri -> {
                    switch (converterRadioGroup.getCheckedRadioButtonId()) {
                        case R.id.radio_file:
                            return RxImageConverters.uriToFile(UpdateProductActivity.this, uri, createTempFile());
                        case R.id.radio_bitmap:
                            return RxImageConverters.uriToBitmap(UpdateProductActivity.this, uri);
                        default:
                            return Observable.just(uri);
                    }
                })
                .subscribe(this::onImagePicked, throwable -> Toast.makeText(UpdateProductActivity.this, String.format("Error: %s", throwable), Toast.LENGTH_LONG).show());
    }

    private void onImagePicked(Object result) {
        if (result instanceof Bitmap) {
            //ivImage.setImageBitmap((Bitmap) result);
        }else{
            photoUri = Uri.parse(String.valueOf(result));

            tempFile = new File(String.valueOf(photoUri));

            compressedImage = compressFoto(UpdateProductActivity.this, tempFile);


            try {
                finalPhotoUri = convertFileToContentUri(UpdateProductActivity.this, compressedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }

            photoExt = "."+getMimeTypeFromUri(UpdateProductActivity.this, finalPhotoUri);
            encodePhoto = encodeFileBase64(getPath(UpdateProductActivity.this, finalPhotoUri));

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    if (Looper.myLooper() == null) {
                        Looper.prepare();
                    }

                    try {
                        theBitmap = Glide.
                                with(UpdateProductActivity.this).
                                load(getPath(UpdateProductActivity.this, finalPhotoUri)).
                                asBitmap().
                                into(100,100).
                                get();
                    } catch (final ExecutionException e) {
                        Log.e("TAG","ExecutionException " + e.getMessage());
                    } catch (final InterruptedException e) {
                        Log.e("TAG","InterruptedException " + e.getMessage());
                    }
                    return null;
                }
                @Override
                protected void onPostExecute(Void dummy) {
                    if (null != theBitmap) {
                        // The full bitmap should be available here
                        //ivAvatar.setImageBitmap(theBitmap);

                        File mypath=new File(IMAGES_DIR,id+".jpeg");

                        ContextWrapper cw = new ContextWrapper(UpdateProductActivity.this);
                        // path to /data/data/yourapp/app_data/imageDir
                        // Create imageDir
                        //File mypath=new File(fotoPath,userId+".jpeg");

                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(mypath);
                            // Use the compress method on the BitMap object to write image to the OutputStream
                            theBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Glide.with(getApplicationContext())
                                .load(mypath)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.ic_preview)
                                .into(ivImage);

                        Log.d("TAG", "Image loaded");
                    };
                }
            }.execute();

            deleteRecursive(new File(String.valueOf(finalPhotoUri)));
            deleteRecursive(createTempFile());
            deleteRecursive(tempFile);

        }
    }

    private File createTempFile() {
        return new File(BASE_DIR + EXTERNAL_DIR_FILES, id + ".jpeg");
    }

    private void getCategory(){
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.information));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        JSONObject jsonObject = new JSONObject();
        AndroidNetworking.get(BASE_IP+"category/readAll")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            String message = response.getString("message");
                            String records = response.getString("records");

                            if (message.equals("success")){
                                JSONArray arr = new JSONArray(records);

                                List<Category> list = new ArrayList<>();

                                Category category = new Category();
                                category.setId("0");
                                category.setCategory(getString(R.string.category));
                                list.add(category);

                                for (int i=0;i<arr.length();i++){
                                    Category resp = gson.fromJson(arr.getJSONObject(i).toString(), Category.class);
                                    list.add(resp);
                                }

                                spinAdapter = new SpinnerAdapter(UpdateProductActivity.this, R.layout.list_item_spinner, list);
                                spin.setAdapter(spinAdapter);

                                if (categoryId!=null && !category.equals("")){
                                    for (int i=0;i<list.size();i++){
                                        if (list.get(i).getId().equals(categoryId))
                                            spin.setSelection(i);
                                    }
                                }

                                spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        //Toast.makeText(getApplicationContext(), list.get(i).getCategory() + " " + list.get(i).getId(), Toast.LENGTH_SHORT).show();
                                        catId = list.get(i).getId();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }else{
                                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
                            }

                            progress.dismiss();
                        } catch (JSONException e) {
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),String.valueOf(e), Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(),String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void insertNew(){

        if (catId.equals("0")){
            Toast.makeText(getApplicationContext(), getString(R.string.category_required), Toast.LENGTH_SHORT).show();
        }else if (txtProductName.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.product_name_required), Toast.LENGTH_SHORT).show();
        }else if (txtDesc.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.product_name_required), Toast.LENGTH_SHORT).show();
        }else if (finalPrice==0.0){
            Toast.makeText(getApplicationContext(), getString(R.string.price_required), Toast.LENGTH_SHORT).show();
        }else if (numberPicker.getValue()==0){
            Toast.makeText(getApplicationContext(), getString(R.string.amount_required), Toast.LENGTH_SHORT).show();
        }else if (encodePhoto.equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.product_image_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("category_id", catId);
                jsonObject.put("seller_id", getUserId());
                jsonObject.put("name", txtProductName.getText().toString());
                jsonObject.put("description", txtDesc.getText().toString());
                jsonObject.put("qty", String.valueOf(numberPicker.getValue()));
                jsonObject.put("price", String.valueOf(finalPrice));
                jsonObject.put("image", id+photoExt);
                jsonObject.put("product_file", getString(R.string.base64_info)+encodePhoto);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"product/create")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            User resp = gson.fromJson(response.toString(), User.class);
                            if (resp.getMessage().equals("success")){
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                                intentToMain();

                            }else{
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }


    }

    private void update(){

        if (catId.equals("0")){
            Toast.makeText(getApplicationContext(), getString(R.string.category_required), Toast.LENGTH_SHORT).show();
        }else if (txtProductName.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.product_name_required), Toast.LENGTH_SHORT).show();
        }else if (txtDesc.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.product_name_required), Toast.LENGTH_SHORT).show();
        }else if (finalPrice==0.0){
            Toast.makeText(getApplicationContext(), getString(R.string.price_required), Toast.LENGTH_SHORT).show();
        }else if (numberPicker.getValue()==0){
            Toast.makeText(getApplicationContext(), getString(R.string.amount_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject jsonObject = new JSONObject();
            try {

                //Log.e("image", id);

                jsonObject.put("id", productId);
                jsonObject.put("category_id", catId);
                jsonObject.put("seller_id", getUserId());
                jsonObject.put("name", txtProductName.getText().toString());
                jsonObject.put("description", txtDesc.getText().toString());
                jsonObject.put("qty", String.valueOf(numberPicker.getValue()));
                jsonObject.put("price", String.valueOf(finalPrice));
                jsonObject.put("image", id);
                jsonObject.put("product_file", getString(R.string.base64_info)+encodePhoto);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"product/update")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            User resp = gson.fromJson(response.toString(), User.class);
                            if (resp.getMessage().equals("success")){
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                                intentToMain();

                            }else{
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }


    }

    private void getData(String pid) {

        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.fetch_data_from_server));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();

        AndroidNetworking.post(BASE_IP+"product/readById")
                .addBodyParameter("id", pid)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            if (response.getString("message").equals("success")){

                                Product product = gson.fromJson(response.toString(), Product.class);

                                if (product.getRate() != null && !product.getRate().equals(""))
                                    rate = Float.parseFloat(product.getRate());
                                else
                                    rate = 0f;

                                if (product.getQty() != null && !product.getQty().equals(""))
                                    stock = Integer.parseInt(product.getQty());
                                else
                                    stock = 0;

                                price = product.getPrice();

                                txtProductName.setText(product.getName());
                                txtDesc.setText(product.getDescription());


                                String originalString = product.getPrice().toString();

                                Long longval;
                                if (originalString.contains(",")) {
                                    originalString = originalString.replaceAll(",", "");

                                    finalPrice = Double.parseDouble(originalString);
                                }
                                longval = Long.parseLong(originalString);

                                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                                formatter.applyPattern("#,###,###,###");
                                String formattedString = formatter.format(longval);

                                //setting text after format to EditText
                                txtPrice.setText(formattedString);
                                txtPrice.setSelection(txtPrice.getText().length());


                                //txtPrice.setText(getPriceInIdr(String.valueOf(price)));
                                numberPicker.setValue(Integer.parseInt(product.getQty()));

                                sellerId = product.getSeller_id();
                                productId = product.getId();
                                categoryId = product.getCategory_id();

                                id = "";
                                //id = getFileNameFromURL(product.getImage().replaceFirst("[.][^.]+$", ""));
                                id = getFileNameFromURL(product.getImage());

                                Glide.with(getApplicationContext())
                                        .load(product.getImage())
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .centerCrop()
                                        .error(R.drawable.ic_preview)
                                        .into(ivImage);
                            }else{
                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progress.dismiss();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void resetData(){
        ivImage.setImageResource(R.drawable.ic_preview);
        photoUri = null;
        finalPhotoUri = null;
        compressedImage = null;
        tempFile= null;
        photoExt = "";
        encodePhoto = "";
        theBitmap = null;
    }

    private void alertDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.delete_question));
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                arg0.dismiss();
                delete();
            }
        });

        alertDialogBuilder.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void delete(){
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.information));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", productId);
            jsonObject.put("status", "deleted");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(BASE_IP+"product/delete")
                .addJSONObjectBody(jsonObject) // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        User resp = gson.fromJson(response.toString(), User.class);
                        if (resp.getMessage().equals("success")){
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            intentToMain();
                        }else{
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void intentToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        intentToMain();
    }
}

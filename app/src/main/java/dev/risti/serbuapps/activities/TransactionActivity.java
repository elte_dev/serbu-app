package dev.risti.serbuapps.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.adapter.CartAdapter;
import dev.risti.serbuapps.adapter.TransactionAdapter;
import dev.risti.serbuapps.imagepicker.RxImageConverters;
import dev.risti.serbuapps.imagepicker.RxImagePicker;
import dev.risti.serbuapps.imagepicker.Sources;
import dev.risti.serbuapps.interfaces.OnJSONResponseCallback;
import dev.risti.serbuapps.interfaces.PaginationAdapterCallback;
import dev.risti.serbuapps.interfaces.PaginationScrollListenerLinear;
import dev.risti.serbuapps.interfaces.RecyclerviewClick;
import dev.risti.serbuapps.model.Notification;
import dev.risti.serbuapps.model.Transaction;
import dev.risti.serbuapps.model.TransactionDetail;
import dev.risti.serbuapps.utils.SharedPref;
import io.reactivex.Observable;

import static dev.risti.serbuapps.MyApp.getContext;
import static dev.risti.serbuapps.utils.GlobalHelper.compressFoto;
import static dev.risti.serbuapps.utils.GlobalHelper.convertFileToContentUri;
import static dev.risti.serbuapps.utils.GlobalHelper.deleteRecursive;
import static dev.risti.serbuapps.utils.GlobalHelper.encodeFileBase64;
import static dev.risti.serbuapps.utils.GlobalHelper.getAddress;
import static dev.risti.serbuapps.utils.GlobalHelper.getMimeTypeFromUri;
import static dev.risti.serbuapps.utils.GlobalHelper.getPath;
import static dev.risti.serbuapps.utils.GlobalHelper.getRole;
import static dev.risti.serbuapps.utils.GlobalHelper.getTransactionId;
import static dev.risti.serbuapps.utils.GlobalHelper.write;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_DIR;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.EXTERNAL_DIR_FILES;
import static dev.risti.serbuapps.utils.GlobalVars.IMAGES_DIR;

public class TransactionActivity extends AppCompatActivity implements PaginationAdapterCallback {

    private TransactionAdapter adapter;

    private Gson gson;

    LinearLayoutManager linearLayoutManager;
    //GridLayoutManager gridLayoutManager;

    private RecyclerView rv;
    private ProgressBar progressBar;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private LinearLayout parentLayout;
    //private MaterialSearchView searchView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvNoData;

    private TextView txtTotal;
    private RadioGroup rgAddress;
    private RadioButton rbAddress;
    private RadioButton rbOtherAddress;
    private EditText txtAddress;
    private EditText txtNote;
    private RadioGroup rgPaymentMethod;
    private RadioButton rbPayment;
    private RadioButton rbTransfer;
    private TextView tvOrder;
    private TextView tvAddressTitle;

    private TextInputLayout tilTranscationId;
    private TextInputLayout tilStatus;
    private EditText txtTransactionId;
    private EditText txtStatus;

    private String paymentMethod = "";

    private List<TransactionDetail> finalList;
    private Notification notification;
    private Transaction transaction;
    private String transactionId = "0";
    private String createdAt = "0";
    private String buyerId = "0";
    private String tempStatus = "";

    private LinearLayout layoutImage;
    //open camera
    private RadioGroup converterRadioGroup;
    private Uri photoUri = null;
    private Uri finalPhotoUri = null;
    private File compressedImage = null;
    private File tempFile= null;
    private String photoExt = "";
    private String encodePhoto = "";
    private Bitmap theBitmap = null;
    private ImageView ivCamera;
    private ImageView ivGalery;
    private ImageView ivImage;
    private String imageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);

        gson = new Gson();

        finalList = new ArrayList<>();

        if (getIntent().hasExtra("data_transaksi2")) {
            transaction = getIntent().getParcelableExtra("data_transaksi2");
            transactionId = transaction.getTransaction_id();
            createdAt = transaction.getCreated_at();
        }else{
            notification = getIntent().getParcelableExtra("data_transaksi");
            transactionId = notification.getTransaction_id();
            createdAt = notification.getCreated_at();
        }

        rv = findViewById(R.id.rv);
        progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);
        parentLayout = findViewById(R.id.parentLayout);
        //searchView = findViewById(R.id.search_view);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        tvNoData = findViewById(R.id.tvNoData);
        txtTotal = findViewById(R.id.txtTotal);
        rgPaymentMethod = findViewById(R.id.rgPaymentMethod);
        rgAddress = findViewById(R.id.rgAddress);
        txtAddress = findViewById(R.id.txtAddress);
        txtNote = findViewById(R.id.txtNote);

        rbPayment = rgPaymentMethod.findViewById(rgPaymentMethod.getCheckedRadioButtonId());
        rbAddress = rgAddress.findViewById(rgAddress.getCheckedRadioButtonId());
        tvOrder = findViewById(R.id.tvOrder);
        tvAddressTitle = findViewById(R.id.tvAddressTitle);

        txtTransactionId = findViewById(R.id.txtTransactionId);
        txtStatus = findViewById(R.id.txtStatus);

        layoutImage = findViewById(R.id.layoutImage);
        ivImage = findViewById(R.id.ivImage);
        ivCamera = findViewById(R.id.ivCamera);
        ivGalery = findViewById(R.id.ivGalery);
        //open camera
        converterRadioGroup = findViewById(R.id.radio_group);
        converterRadioGroup.check(R.id.radio_file);

        if (RxImagePicker.with(TransactionActivity.this).getActiveSubscription() != null) {
            RxImagePicker.with(TransactionActivity.this).getActiveSubscription().subscribe(this::onImagePicked);
        }

        imageId = String.valueOf(System.currentTimeMillis());
        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromSource(Sources.CAMERA);
            }
        });

        ivGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromSource(Sources.GALLERY);
            }
        });

        tilTranscationId = findViewById(R.id.tilTranscationId);
        tilStatus = findViewById(R.id.tilStatus);
        tilTranscationId.setVisibility(View.VISIBLE);
        tilStatus.setVisibility(View.VISIBLE);

        tvAddressTitle.setVisibility(View.GONE);
        rgAddress.setVisibility(View.GONE);

        tvOrder.setVisibility(View.GONE);
        tvOrder.setText(R.string.confirm);

        tvOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTransaction();
            }
        });

        for(int i = 0; i < rgAddress.getChildCount(); i++){
            (rgAddress.getChildAt(i)).setEnabled(false);
        }

        for(int i = 0; i < rgPaymentMethod.getChildCount(); i++){
            (rgPaymentMethod.getChildAt(i)).setEnabled(false);
        }

        txtStatus.setEnabled(false);
        txtAddress.setEnabled(false);
        txtNote.setEnabled(false);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        adapter = new TransactionAdapter(this, new RecyclerviewClick() {
            @Override
            public void onItemClick(View v, int position) {

            }
        });

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        //gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),
                linearLayoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.rv_divider));
        rv.addItemDecoration(dividerItemDecoration);

        rv.setLayoutManager(linearLayoutManager);

        //rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFirstPage(transactionId);
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                loadFirstPage(transactionId);
            }
        });

        rv.addOnScrollListener(new PaginationScrollListenerLinear(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {

            }

            @Override
            public int getTotalPageCount() {
                return 1;
            }

            @Override
            public boolean isLastPage() {
                return true;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFirstPage(transactionId);
            }
        });
    }

    private void pickImageFromSource(Sources source) {

        RxImagePicker.with(TransactionActivity.this).requestImage(source)
                .flatMap(uri -> {
                    switch (converterRadioGroup.getCheckedRadioButtonId()) {
                        case R.id.radio_file:
                            return RxImageConverters.uriToFile(TransactionActivity.this, uri, createTempFile());
                        case R.id.radio_bitmap:
                            return RxImageConverters.uriToBitmap(TransactionActivity.this, uri);
                        default:
                            return Observable.just(uri);
                    }
                })
                .subscribe(this::onImagePicked, throwable -> Toast.makeText(TransactionActivity.this, String.format("Error: %s", throwable), Toast.LENGTH_LONG).show());
    }

    private File createTempFile() {
        return new File(BASE_DIR + EXTERNAL_DIR_FILES, imageId + ".jpeg");
    }

    private void onImagePicked(Object result) {
        if (result instanceof Bitmap) {
            //ivImage.setImageBitmap((Bitmap) result);
        }else{
            photoUri = Uri.parse(String.valueOf(result));

            tempFile = new File(String.valueOf(photoUri));

            compressedImage = compressFoto(TransactionActivity.this, tempFile);


            try {
                finalPhotoUri = convertFileToContentUri(TransactionActivity.this, compressedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }

            photoExt = "."+getMimeTypeFromUri(TransactionActivity.this, finalPhotoUri);
            encodePhoto = encodeFileBase64(getPath(TransactionActivity.this, finalPhotoUri));

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    if (Looper.myLooper() == null) {
                        Looper.prepare();
                    }

                    try {
                        theBitmap = Glide.
                                with(TransactionActivity.this).
                                load(getPath(TransactionActivity.this, finalPhotoUri)).
                                asBitmap().
                                into(100,100).
                                get();
                    } catch (final ExecutionException e) {
                        Log.e("TAG","ExecutionException " + e.getMessage());
                    } catch (final InterruptedException e) {
                        Log.e("TAG","InterruptedException " + e.getMessage());
                    }
                    return null;
                }
                @Override
                protected void onPostExecute(Void dummy) {
                    if (null != theBitmap) {
                        // The full bitmap should be available here
                        //ivAvatar.setImageBitmap(theBitmap);

                        File mypath=new File(IMAGES_DIR,imageId+".jpeg");

                        ContextWrapper cw = new ContextWrapper(TransactionActivity.this);
                        // path to /data/data/yourapp/app_data/imageDir
                        // Create imageDir
                        //File mypath=new File(fotoPath,userId+".jpeg");

                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(mypath);
                            // Use the compress method on the BitMap object to write image to the OutputStream
                            theBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Glide.with(getApplicationContext())
                                .load(mypath)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.ic_preview)
                                .into(ivImage);

                        Log.d("TAG", "Image loaded");
                    };
                }
            }.execute();

            deleteRecursive(new File(String.valueOf(finalPhotoUri)));
            deleteRecursive(createTempFile());
            deleteRecursive(tempFile);

        }
    }

    private void loadFirstPage(String key) {

        if (progressBar!=null && progressBar.getVisibility()==View.GONE)
            progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
        hideErrorView();

        if (adapter!=null && !adapter.isEmpty())
            adapter.clearAll();

        String url = BASE_IP+"transaction_detail/readById";

        AndroidNetworking.post(url)
                .setPriority(Priority.LOW)
                .addBodyParameter("key", key)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideErrorView();

                        if (finalList != null && !finalList.isEmpty())
                            finalList.clear();

                        List<TransactionDetail> results = new ArrayList<>();

                        String status = "booked";
                        int total =0;

                        Transaction transaction = gson.fromJson(response.toString(), Transaction.class);

                        if (transaction.getTransactionDetail()!=null && !transaction.getTransactionDetail().isEmpty()){
                            results = transaction.getTransactionDetail();

                            for (TransactionDetail detail:results){
                                total += (Integer.parseInt(detail.getPrice())*Integer.parseInt(detail.getQty()));

                                status = detail.getStatus();
                                buyerId = detail.getBuyer_id();
                            }

                        }

                        //Log.e("pay", transaction.getPayment_method());

                        if (transaction.getPayment_method().equals("cod")){
                            if (getRole().equals("buyer")){
                                if (status.equals("delivered")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.done_confirm);
                                    tempStatus = "done";
                                }else if (status.equals("done")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.rate_product);
                                    tempStatus = "rated";
                                }else{
                                    tvOrder.setVisibility(View.GONE);
                                }
                            }else{
                                if (status.equals("booked")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.confirm);
                                    tempStatus = "confirmed";
                                }else  if (status.equals("confirmed")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.on_process_confirm);
                                    tempStatus = "on_process";
                                }else if (status.equals("on_process")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.delivered_confirm);
                                    tempStatus = "delivered";
                                }else{
                                    tvOrder.setVisibility(View.GONE);
                                }
                            }
                        }else{
                            if (getRole().equals("seller")){
                                if (status.equals("booked")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.confirmed);
                                    tempStatus = "confirmed";
                                }else  if (status.equals("transferred")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.on_process_confirm);
                                    tempStatus = "on_process";
                                }else  if (status.equals("on_process")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.delivered_confirm);
                                    tempStatus = "delivered";
                                }else{
                                    tvOrder.setVisibility(View.GONE);
                                }
                            }else{
                                if (status.equals("confirmed")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.confirmed);
                                    layoutImage.setVisibility(View.VISIBLE);
                                    tempStatus = "transferred";
                                }else if (status.equals("delivered")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.done_confirm);
                                    tempStatus = "done";
                                }else if (status.equals("done")){
                                    tvOrder.setVisibility(View.VISIBLE);
                                    tvOrder.setText(R.string.rate_product);
                                    tempStatus = "rated";
                                }else{
                                    tvOrder.setVisibility(View.GONE);
                                }
                            }
                        }



                        txtTransactionId.setText(getTransactionId(transaction.getTransaction_id(), transaction.getCreated_at()));
                        txtStatus.setText(status);
                        txtAddress.setText(transaction.getAddress());
                        txtNote.setText(transaction.getTransaction_note());
                        txtTotal.setText(String.valueOf(total));

                        if (transaction.getPayment_method().equals("cod")){
                            rgPaymentMethod.check(R.id.rbCod);
                            paymentMethod = "cod";
                        }else if (transaction.getPayment_method().equals("transfer")){
                            rgPaymentMethod.check(R.id.rbTransfer);
                            paymentMethod = "transfer";
                        }

                        finalList = results;

                        if (results.isEmpty()){
                            tvNoData.setVisibility(View.VISIBLE);
                            rv.setVisibility(View.GONE);
                        }else{
                            tvNoData.setVisibility(View.GONE);
                            rv.setVisibility(View.VISIBLE);
                        }


                        //List<Po> results = fetchResults(response);
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.addAll(results);

                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        showErrorView(error);
                    }
                });
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void retryPageLoad() {
        loadFirstPage(transactionId);
    }

    public void updateTransaction(){
        if (txtTotal.getText().toString().equals("0")){
            Toast.makeText(getApplicationContext(), getString(R.string.item_required), Toast.LENGTH_SHORT).show();
        }else if (txtAddress.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.address_required), Toast.LENGTH_SHORT).show();
        }else if (getRole().equals("buyer") && tempStatus.equals("transferred") && (encodePhoto==null || encodePhoto.equals(""))){
            Toast.makeText(getApplicationContext(), getString(R.string.proof_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("id", transactionId);
                jsonObject.put("buyer_id", buyerId);
                jsonObject.put("payment_method", paymentMethod);
                jsonObject.put("note", txtNote.getText().toString());
                jsonObject.put("proof_of_payment", transactionId +photoExt);
                jsonObject.put("proof_file", getString(R.string.base64_info) + encodePhoto);
                jsonObject.put("status", "booked");
                jsonObject.put("created_at", createdAt);
                jsonObject.put("address", 	txtAddress.getText().toString());
                JSONArray childArr = new JSONArray();

                for (TransactionDetail detail :  finalList){
                    JSONObject childObj = new JSONObject();
                    childObj.put("id", detail.getId());
                    childObj.put("seller_id", detail.getSeller_id());
                    childObj.put("buyer_id", detail.getBuyer_id());
                    childObj.put("product_id", detail.getProduct_id());
                    childObj.put("qty", detail.getQty());
                    childObj.put("price", detail.getPrice());
                    childObj.put("note", detail.getNote());
                    childObj.put("rate", detail.getRate());
                    childObj.put("status", tempStatus);
                    childArr.put(childObj);
                }

                jsonObject.put("item", childArr);

                write(getApplicationContext(), "aaa.txt", jsonObject.toString(1));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"transaction_detail/update_status")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            TransactionDetail resp = gson.fromJson(response.toString(), TransactionDetail.class);
                            if (resp.getMessage().equals("success")){
                                progress.dismiss();
                                loadFirstPage(transactionId);
                            }else{
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }


    }

    private void intentToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();
        intentToMain();

    }
}

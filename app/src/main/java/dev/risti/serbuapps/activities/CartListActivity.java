package dev.risti.serbuapps.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.adapter.CartAdapter;
import dev.risti.serbuapps.interfaces.OnJSONResponseCallback;
import dev.risti.serbuapps.interfaces.PaginationAdapterCallback;
import dev.risti.serbuapps.interfaces.PaginationScrollListenerGrid;
import dev.risti.serbuapps.interfaces.PaginationScrollListenerLinear;
import dev.risti.serbuapps.interfaces.RecyclerviewClick;
import dev.risti.serbuapps.model.Notification;
import dev.risti.serbuapps.model.TransactionDetail;
import dev.risti.serbuapps.model.User;
import dev.risti.serbuapps.utils.SharedPref;

import static dev.risti.serbuapps.MyApp.getContext;
import static dev.risti.serbuapps.utils.GlobalHelper.convertDate;
import static dev.risti.serbuapps.utils.GlobalHelper.getAddress;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserId;
import static dev.risti.serbuapps.utils.GlobalHelper.write;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_CART_COUNTER;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_ID;

public class CartListActivity extends AppCompatActivity implements PaginationAdapterCallback {

    private CartAdapter adapter;

    private Gson gson;

    LinearLayoutManager linearLayoutManager;
    //GridLayoutManager gridLayoutManager;

    private RecyclerView rv;
    private ProgressBar progressBar;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private LinearLayout parentLayout;
    //private MaterialSearchView searchView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvNoData;

    private TextView txtTotal;
    private RadioGroup rgAddress;
    private RadioButton rbAddress;
    private RadioButton rbOtherAddress;
    private EditText txtAddress;
    private EditText txtNote;
    private RadioGroup rgPaymentMethod;
    private RadioButton rbPayment;
    private RadioButton rbTransfer;
    private TextView tvOrder;

    private static final int PAGE_START = 1;

    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;

    private SharedPref sharedPref;
    private String paymentMethod = "";

    List<TransactionDetail> finalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);

        gson = new Gson();
        sharedPref = new SharedPref(this);
        finalList = new ArrayList<>();


        rv = findViewById(R.id.rv);
        progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);
        parentLayout = findViewById(R.id.parentLayout);
        //searchView = findViewById(R.id.search_view);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        tvNoData = findViewById(R.id.tvNoData);
        txtTotal = findViewById(R.id.txtTotal);
        rgPaymentMethod = findViewById(R.id.rgPaymentMethod);
        rgAddress = findViewById(R.id.rgAddress);
        txtAddress = findViewById(R.id.txtAddress);
        txtNote = findViewById(R.id.txtNote);

        rbPayment = rgPaymentMethod.findViewById(rgPaymentMethod.getCheckedRadioButtonId());
        rbAddress = rgAddress.findViewById(rgAddress.getCheckedRadioButtonId());
        tvOrder = findViewById(R.id.tvOrder);

        tvOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createTransaction();
            }
        });

        rgPaymentMethod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbCod) {
                    paymentMethod = "cod";
                } else if (checkedId == R.id.rbTransfer) {
                    paymentMethod = "transfer";
                }
            }
        });

        rgAddress.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbCurrentAddress) {
                    txtAddress.setText("");
                    txtAddress.setText(getAddress());
                } else if (checkedId == R.id.rbOtherAddress) {
                    txtAddress.setText("");
                }
            }
        });

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        adapter = new CartAdapter(this, txtTotal, new RecyclerviewClick() {
            @Override
            public void onItemClick(View v, int position) {

            }
        });

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        //gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rv.getContext(),
                linearLayoutManager.getOrientation());
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(getContext(), R.drawable.rv_divider));
        rv.addItemDecoration(dividerItemDecoration);

        rv.setLayoutManager(linearLayoutManager);

        //rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFirstPage();
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                loadFirstPage();
            }
        });

//        rv.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount) {
//                isLoading = true;
//                currentPage = currentPage + 1;
//
//                loadNextPage(id);
//            }
//        });

        rv.addOnScrollListener(new PaginationScrollListenerLinear(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage = currentPage + 1;

                loadNextPage();
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFirstPage();
            }
        });
    }

    private void loadFirstPage() {

        if (progressBar!=null && progressBar.getVisibility()==View.GONE)
            progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
        hideErrorView();

        if (adapter!=null && !adapter.isEmpty())
            adapter.clearAll();

        currentPage = 1;
        isLastPage = false;

        String url = BASE_IP+"transaction_detail/readByUser?key="+ getUserId() + "&page=";

        AndroidNetworking.get(url+PAGE_START)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideErrorView();

                        if (finalList != null && !finalList.isEmpty())
                            finalList.clear();

                        List<TransactionDetail> results = new ArrayList<>();
                        try {
                            String records = response.getString("records");
                            String paging = response.getString("paging");
                            JSONObject pageObj = new JSONObject(paging);

                            TOTAL_PAGES = pageObj.getInt("total_pages");

                            JSONArray dataArr = new JSONArray(records);

                            //Log.e("dataArr", dataArr.toString(1));

                            if (dataArr.length()>0){
                                for (int i = 0; i < dataArr.length(); i++) {
                                    TransactionDetail transactionDetail = gson.fromJson(dataArr.getJSONObject(i).toString(), TransactionDetail.class);
                                    results.add(transactionDetail);
                                }
                                sharedPref.putString(KEY_CART_COUNTER, String.valueOf(dataArr.length()));
                                finalList = results;

                            }else{
                                sharedPref.putString(KEY_CART_COUNTER, "");
                            }

                            if (results.isEmpty()){
                                tvNoData.setVisibility(View.VISIBLE);
                                rv.setVisibility(View.GONE);

                            }else{
                                tvNoData.setVisibility(View.GONE);
                                rv.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //List<Po> results = fetchResults(response);
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.addAll(results);


                        if (currentPage <= TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        } else {
                            isLastPage = true;
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        showErrorView(error);
                    }
                });
    }

    private void loadNextPage() {
        hideErrorView();

        String url = BASE_IP+"transaction_detail/readByUser?key="+ getUserId() + "&page=";

        AndroidNetworking.get(url+currentPage)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        // do anything with response
                        hideErrorView();

                        List<TransactionDetail> results = new ArrayList<>();
                        try {
                            String message = response.getString("message");
                            String records = response.getString("records");
                            String paging = response.getString("paging");
                            JSONObject pageObj = new JSONObject(paging);

                            TOTAL_PAGES = pageObj.getInt("total_pages");
                            JSONArray dataArr = new JSONArray(records);

                            if (dataArr.length()>0){
                                for (int i = 0; i < dataArr.length(); i++) {
                                    TransactionDetail transactionDetail = gson.fromJson(dataArr.getJSONObject(i).toString(), TransactionDetail.class);
                                    results.add(transactionDetail);
                                }

                                sharedPref.putString(KEY_CART_COUNTER, String.valueOf(dataArr.length()));
                                finalList = results;
                            }else{
                                sharedPref.putString(KEY_CART_COUNTER, "");
                            }

                            if (results.isEmpty()){
                                tvNoData.setVisibility(View.VISIBLE);
                                rv.setVisibility(View.GONE);
                            }else{
                                tvNoData.setVisibility(View.GONE);
                                rv.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter.removeLoadingFooter();
                        isLoading = false;

                        adapter.addAll(results);

                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        }
                        else {
                            isLastPage = true;
                        }


                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        showErrorView(error);
                    }
                });
    }

    public void createTransaction(){
        if (txtTotal.getText().toString().equals("0")){
            Toast.makeText(getApplicationContext(), getString(R.string.item_required), Toast.LENGTH_SHORT).show();
        }else if (paymentMethod.equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.payment_method_required), Toast.LENGTH_SHORT).show();
        }else if (txtAddress.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.address_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put("buyer_id", getUserId());
                jsonObject.put("payment_method", paymentMethod);
                jsonObject.put("note", txtNote.getText().toString());
                jsonObject.put("proof_of_payment", "");
                jsonObject.put("status", "booked");
                jsonObject.put("proof_file", "");
                jsonObject.put("address", 	txtAddress.getText().toString());
                JSONArray childArr = new JSONArray();

                for (TransactionDetail detail :  finalList){
                    if (detail.isSelected()==true){

                        JSONObject childObj = new JSONObject();
                        childObj.put("id", detail.getId());
                        childObj.put("seller_id", detail.getSeller_id());
                        childObj.put("buyer_id", detail.getBuyer_id());
                        childObj.put("product_id", detail.getProduct_id());
                        childObj.put("qty", detail.getQty());
                        childObj.put("price", detail.getPrice());
                        childObj.put("note", detail.getNote());
                        childObj.put("rate", detail.getRate());
                        childObj.put("status", "booked");
                        childArr.put(childObj);
                    }
                }

                jsonObject.put("item", childArr);

                //write(getApplicationContext(), "aaa.txt", jsonObject.toString(1));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"transaction_detail/update")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            TransactionDetail resp = gson.fromJson(response.toString(), TransactionDetail.class);
                            if (resp.getMessage().equals("success")){
                                progress.dismiss();
                                //loadFirstPage();
                                intentToMain();
                            }else{
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }


    }

    public void upateOrderAmount(String id, String qty, final OnJSONResponseCallback onJSONResponseCallback){
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.information));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("qty", qty);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(BASE_IP+"transaction_detail/update_amount")
                .addJSONObjectBody(jsonObject) // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        TransactionDetail resp = gson.fromJson(response.toString(), TransactionDetail.class);
                        if (resp.getMessage().equals("success")){
                            progress.dismiss();
                            onJSONResponseCallback.onJSONResponse(true);
                        }else{
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            onJSONResponseCallback.onJSONResponse(false);
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        onJSONResponseCallback.onJSONResponse(false);
                        Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void retryPageLoad() {
        loadNextPage();
    }

    private void intentToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void showAlert(){
        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(CartListActivity.this);
        builder.setTitle(getString(R.string.information));
        builder.setMessage(getString(R.string.sure_to_delete));
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.show();
    }


    @Override
    public void onBackPressed(){
        //super.onBackPressed();
        intentToMain();

    }
}

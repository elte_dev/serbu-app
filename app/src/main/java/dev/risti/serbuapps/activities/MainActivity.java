package dev.risti.serbuapps.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.onesignal.OneSignal;

import java.util.ArrayList;
import java.util.List;
import dev.risti.serbuapps.R;
import dev.risti.serbuapps.adapter.MenuAdapter;
import dev.risti.serbuapps.interfaces.RecyclerviewClick;
import dev.risti.serbuapps.location.CurrentLocationListener;
import dev.risti.serbuapps.location.CurrentLocationReceiver;
import dev.risti.serbuapps.location.LocationTracker;
import dev.risti.serbuapps.model.MainMenu;
import dev.risti.serbuapps.utils.SharedPref;

import static dev.risti.serbuapps.utils.GlobalHelper.getRole;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserEmail;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserId;
import static dev.risti.serbuapps.utils.GlobalHelper.setBadgeCount;
import static dev.risti.serbuapps.utils.GlobalVars.GPS_INTERVAL;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_ACTION;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_ACTION_INSERT;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_CART_COUNTER;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_CATEGORY_ID;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_FALSE;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_OWN_PRODUCT;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_TRUE;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_USER_ROLE;
import static dev.risti.serbuapps.utils.GlobalVars.userHash;

public class MainActivity extends AppCompatActivity {

    private String TAG = MainActivity.class.getSimpleName();

    List<MainMenu> list =new ArrayList<>();

    private RecyclerView rv;
    private MenuAdapter adapter;
    private LinearLayout parentLayout;

    private MainMenu menu;
    private Intent intent;

    private LocationTracker locationTracker;
    private double latitude = 0.0;
    private double longitude = 0.0;

    private int mNotificationsCount = 0;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPref = new SharedPref(this);
        if (sharedPref.getString(KEY_CART_COUNTER)!=null && !sharedPref.getString(KEY_CART_COUNTER).equals(""))
            mNotificationsCount = Integer.parseInt(sharedPref.getString(KEY_CART_COUNTER));
        else
            mNotificationsCount = 0;

        rv = findViewById(R.id.rv);
        parentLayout = findViewById(R.id.parentLayout);

        Log.e(TAG, "role "+ getRole());

        if (getRole().equals("buyer")){
            addBuyerMenu();
        }else if (getRole().equals("seller")){
            addSellerMenu();
        }

        initLocation();

        //DemoSyncJob.scheduleJob();
    }


    private void initLocation(){
        locationTracker = new LocationTracker("dev.elte.ActivityRecognition.RestartSensor")
                .setInterval(GPS_INTERVAL)
                .setGps(true)
                .setNetWork(true)
                .currentLocation(new CurrentLocationReceiver(new CurrentLocationListener() {
                    @Override
                    public void onCurrentLocation(Location location) {
//                        Log.d("callback", ":onCurrentLocation" + location.getLongitude());
                        //latitude = location.getLatitude();
                        //longitude = location.getLongitude();

                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                    }

                    @Override
                    public void onPermissionDenied() {
                        Log.d("callback", ":onPermissionDenied");

                    }
                }))
                .start(getBaseContext(), MainActivity.this);
    }


    private void addBuyerMenu(){
        //0
        menu = new MainMenu(getString(R.string.home));
        list.add(menu);
        //1
        menu = new MainMenu(getString(R.string.profile));
        list.add(menu);
        //2
        menu = new MainMenu(getString(R.string.notification));
        list.add(menu);
        //3
        menu = new MainMenu(getString(R.string.cart));
        list.add(menu);
        //4
        menu = new MainMenu(getString(R.string.category));
        list.add(menu);
        //5
        menu = new MainMenu(getString(R.string.purchase));
        list.add(menu);

        adapter = new MenuAdapter(this, list, new RecyclerviewClick() {
            @Override
            public void onItemClick(View v, int position) {
                if (position == 0){
                    intentToProductList(KEY_FALSE);
                }else if (position == 1){
                    intentToProfile();
                }else if (position == 2){
                    intentToNotificationList();
                }else if (position == 3){
                    intentToCart();
                }else if (position == 4){
                    intentToCategory();
                }else if (position == 5){
                    intentToSalesReport();
                }

            }
        });

        rv.setHasFixedSize(true);
        rv.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
    }

    private void addSellerMenu(){
        menu = new MainMenu(getString(R.string.home));
        list.add(menu);
        menu = new MainMenu(getString(R.string.profile));
        list.add(menu);
        menu = new MainMenu(getString(R.string.notification));
        list.add(menu);
        menu = new MainMenu(getString(R.string.product_list));
        list.add(menu);
        menu = new MainMenu(getString(R.string.add_product));
        list.add(menu);
        menu = new MainMenu(getString(R.string.sales));
        list.add(menu);

        adapter = new MenuAdapter(this, list, new RecyclerviewClick() {
            @Override
            public void onItemClick(View v, int position) {
                if (position == 0){
                    intentToProductList(KEY_FALSE);
                }else if (position == 1){
                    intentToProfile();
                }else if (position == 2){
                    intentToNotificationList();
                }else if (position == 3){
                    intentToProductList(KEY_TRUE);
                }else if (position == 4){
                    intentToUpdate(KEY_ACTION_INSERT);
                }else if (position == 5){
                    intentToSalesReport();
                }

            }
        });

        rv.setHasFixedSize(true);
        rv.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu); // inflate your menu resource

        MenuItem item = menu.findItem(R.id.menu_item_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        setBadgeCount(this, icon, mNotificationsCount);

        MenuItem logout = menu.findItem(R.id.menu_item_logout);
        logout.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                exitDialog();
                return false;
            }
        });
        return true;
    }

    private void updateNotificationsBadge(int count) {
        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }

    private void exitDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.exit_question));
        alertDialogBuilder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                userHash.flush();
                finish();
            }
        });

        alertDialogBuilder.setNegativeButton(getString(R.string.no),new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void intentToCategory(){
        intent = new Intent(this, CategoryListActivity.class);
        startActivity(intent);
        finish();
    }

    private void intentToCart(){
        intent = new Intent(this, CartListActivity.class);
        startActivity(intent);
        finish();
    }

    private void intentToProfile(){
        intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(KEY_USER_ROLE, getUserId());
        startActivity(intent);
        finish();
    }

    private void intentToProductList(String ownProduct){
        intent = new Intent(this, ProductListActivity.class);
        intent.putExtra(KEY_OWN_PRODUCT, ownProduct);
        startActivity(intent);
        finish();
    }

    private void intentToNotificationList(){
        intent = new Intent(this, NotificationListActivity.class);
        startActivity(intent);
        finish();
    }

    private void intentToUpdate(String action){
        intent = new Intent(this, UpdateProductActivity.class);
        intent.putExtra(KEY_ACTION, action);
        startActivity(intent);
        finish();
    }

    private void intentToSalesReport(){
        intent = new Intent(this, SalesListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationTracker.stopLocationService(MainActivity.this);
    }
}

package dev.risti.serbuapps.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import co.ceryle.radiorealbutton.RadioRealButton;
import co.ceryle.radiorealbutton.RadioRealButtonGroup;
import dev.risti.serbuapps.R;
import dev.risti.serbuapps.imagepicker.RxImageConverters;
import dev.risti.serbuapps.imagepicker.RxImagePicker;
import dev.risti.serbuapps.imagepicker.Sources;
import dev.risti.serbuapps.model.User;
import dev.risti.serbuapps.slidedatetimepicker.SlideDateTimeListener;
import dev.risti.serbuapps.slidedatetimepicker.SlideDateTimePicker;
import io.reactivex.Observable;

import static dev.risti.serbuapps.utils.GlobalHelper.compressFoto;
import static dev.risti.serbuapps.utils.GlobalHelper.convertDate;
import static dev.risti.serbuapps.utils.GlobalHelper.convertFileToContentUri;
import static dev.risti.serbuapps.utils.GlobalHelper.convertStringToDateFormat;
import static dev.risti.serbuapps.utils.GlobalHelper.deleteRecursive;
import static dev.risti.serbuapps.utils.GlobalHelper.encodeFileBase64;
import static dev.risti.serbuapps.utils.GlobalHelper.getMimeTypeFromUri;
import static dev.risti.serbuapps.utils.GlobalHelper.getPath;
import static dev.risti.serbuapps.utils.GlobalHelper.write;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_DIR;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.EXTERNAL_DIR_FILES;
import static dev.risti.serbuapps.utils.GlobalVars.IMAGES_DIR;
import static dev.risti.serbuapps.utils.GlobalVars.userHash;

public class RegisterActivity extends AppCompatActivity {

    //open camera
    private RadioGroup converterRadioGroup;
    private Uri photoUri = null;
    private Uri finalPhotoUri = null;
    private File compressedImage = null;
    private File tempFile= null;
    private String photoExt = "";
    private String encodePhoto = "";
    private Bitmap theBitmap = null;

    private RadioRealButton rbSeller;
    private RadioRealButton rbBuyer;
    private RadioRealButtonGroup rgRole;

    private LinearLayout layoutContent;
    private LinearLayout layoutImage;
    private EditText txtBirthday;
    private ImageView ivDate;
    private EditText txtName;
    private EditText txtEmail;
    private EditText txtHp;
    private EditText txtAddress;
    private EditText txtPassword;
    private ImageView ivId;
    private ImageView ivCamera;
    private ImageView ivGalery;
    private Button btnRegister;
    private TextView tvLogin;

    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MMM yyyy");

    private String id;
    private String role = "";

    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        gson = new Gson();

        rbSeller = findViewById(R.id.rbSeller);
        rbBuyer = findViewById(R.id.rbBuyer);
        rgRole = findViewById(R.id.rgRole);
        layoutContent = findViewById(R.id.layoutContent);
        layoutImage = findViewById(R.id.layoutImage);
        txtBirthday = findViewById(R.id.txtBirthday);
        ivDate = findViewById(R.id.ivDate);
        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        txtHp = findViewById(R.id.txtHp);
        txtAddress = findViewById(R.id.txtAddress);
        txtPassword = findViewById(R.id.txtPassword);
        ivId = findViewById(R.id.ivId);
        ivCamera = findViewById(R.id.ivCamera);
        ivGalery = findViewById(R.id.ivGalery);
        btnRegister = findViewById(R.id.btnRegister);
        tvLogin = findViewById(R.id.tvLogin);

        //open camera
        converterRadioGroup = findViewById(R.id.radio_group);
        converterRadioGroup.check(R.id.radio_file);

        if (RxImagePicker.with(RegisterActivity.this).getActiveSubscription() != null) {
            RxImagePicker.with(RegisterActivity.this).getActiveSubscription().subscribe(this::onImagePicked);
        }

        txtBirthday.setText(mFormatter.format(new Date()));

        id = String.valueOf(System.currentTimeMillis());

        ivDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newDate = convertDate(txtBirthday.getText().toString(), "dd MMM yyyy", "EEE MMM HH:mm:ss z yyyy");

                Date date = convertStringToDateFormat(newDate, "EEE MMM HH:mm:ss z yyyy");

                //Log.e("new Date()", String.valueOf(new Date()));

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(listener)
                        .setInitialDate(date)
                        //.setMinDate(minDate)
                        // .setMaxDate(maxDate)
                        .setIs24HourTime(true)
                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
                        // .setIndicatorColor(Color.parseColor("#990000"))
                        .build()
                        .show();
            }
        });

        rgRole.setOnClickedButtonListener(new RadioRealButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(RadioRealButton button, int position) {
                layoutContent.setVisibility(View.VISIBLE);

                TranslateAnimation animate = new TranslateAnimation(
                        0,                 // fromXDelta
                        0,                 // toXDelta
                        layoutContent.getHeight(),  // fromYDelta
                        0);                // toYDelta
                animate.setDuration(500);
                animate.setFillAfter(true);
                layoutContent.startAnimation(animate);

                if (position==0){
                    role = "seller";
                    resetData();
                    layoutImage.setVisibility(View.VISIBLE);
                    TranslateAnimation animateImage = new TranslateAnimation(
                            0,                 // fromXDelta
                            0,                 // toXDelta
                            layoutContent.getHeight(),  // fromYDelta
                            0);                // toYDelta
                    animateImage.setDuration(500);
                    animateImage.setFillAfter(true);
                    layoutImage.startAnimation(animateImage);
                }else{
                    role = "buyer";
                    resetData();
                    layoutImage.setVisibility(View.GONE);
                    TranslateAnimation animateImage = new TranslateAnimation(
                            0,                 // fromXDelta
                            0,                 // toXDelta
                            0,                 // fromYDelta
                            layoutImage.getHeight()); // toYDelta
                    animateImage.setDuration(500);
                    animateImage.setFillAfter(true);
                    layoutImage.startAnimation(animateImage);
                }

            }
        });

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromSource(Sources.CAMERA);
            }
        });

        ivGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromSource(Sources.GALLERY);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (role.equals("seller")){
                    insertSeller();
                }else{
                    insertBuyer();
                }
            }
        });
    }

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
            txtBirthday.setText(mFormatter.format(date));
        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel() {
        }
    };

    private void pickImageFromSource(Sources source) {

        RxImagePicker.with(RegisterActivity.this).requestImage(source)
                .flatMap(uri -> {
                    switch (converterRadioGroup.getCheckedRadioButtonId()) {
                        case R.id.radio_file:
                            return RxImageConverters.uriToFile(RegisterActivity.this, uri, createTempFile());
                        case R.id.radio_bitmap:
                            return RxImageConverters.uriToBitmap(RegisterActivity.this, uri);
                        default:
                            return Observable.just(uri);
                    }
                })
                .subscribe(this::onImagePicked, throwable -> Toast.makeText(RegisterActivity.this, String.format("Error: %s", throwable), Toast.LENGTH_LONG).show());
    }

    private File createTempFile() {
        return new File(BASE_DIR + EXTERNAL_DIR_FILES, id + ".jpeg");
    }

    private void onImagePicked(Object result) {
        if (result instanceof Bitmap) {
            //ivImage.setImageBitmap((Bitmap) result);
        }else{
            photoUri = Uri.parse(String.valueOf(result));

            tempFile = new File(String.valueOf(photoUri));

            compressedImage = compressFoto(RegisterActivity.this, tempFile);


            try {
                finalPhotoUri = convertFileToContentUri(RegisterActivity.this, compressedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }

            photoExt = "."+getMimeTypeFromUri(RegisterActivity.this, finalPhotoUri);
            encodePhoto = encodeFileBase64(getPath(RegisterActivity.this, finalPhotoUri));

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    if (Looper.myLooper() == null) {
                        Looper.prepare();
                    }

                    try {
                        theBitmap = Glide.
                                with(RegisterActivity.this).
                                load(getPath(RegisterActivity.this, finalPhotoUri)).
                                asBitmap().
                                into(100,100).
                                get();
                    } catch (final ExecutionException e) {
                        Log.e("TAG","ExecutionException " + e.getMessage());
                    } catch (final InterruptedException e) {
                        Log.e("TAG","InterruptedException " + e.getMessage());
                    }
                    return null;
                }
                @SuppressLint("WrongThread")
                @Override
                protected void onPostExecute(Void dummy) {
                    if (null != theBitmap) {
                        // The full bitmap should be available here
                        //ivAvatar.setImageBitmap(theBitmap);

                        File mypath=new File(IMAGES_DIR,id+".jpeg");

                        ContextWrapper cw = new ContextWrapper(RegisterActivity.this);
                        // path to /data/data/yourapp/app_data/imageDir
                        // Create imageDir
                        //File mypath=new File(fotoPath,userId+".jpeg");

                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(mypath);
                            // Use the compress method on the BitMap object to write image to the OutputStream
                            theBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Glide.with(getApplicationContext())
                                .load(mypath)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.ic_preview)
                                .into(ivId);

                        Log.d("TAG", "Image loaded");
                    };
                }
            }.execute();

            deleteRecursive(new File(String.valueOf(finalPhotoUri)));
            deleteRecursive(createTempFile());
            deleteRecursive(tempFile);

        }
    }

    private void resetData(){
        txtBirthday.setText(mFormatter.format(new Date()));
        txtName.setText("");
        txtEmail.setText("");
        txtHp.setText("");
        txtAddress.setText("");
        txtPassword.setText("");
        ivId.setImageResource(R.drawable.ic_preview);
        photoUri = null;
        finalPhotoUri = null;
        compressedImage = null;
        tempFile= null;
        photoExt = "";
        encodePhoto = "";
        theBitmap = null;
    }

    private void insertSeller(){

        if (txtName.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.name_required), Toast.LENGTH_SHORT).show();
        }else if (txtEmail.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.email_required), Toast.LENGTH_SHORT).show();
        }else if (txtHp.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.mobile_required), Toast.LENGTH_SHORT).show();
        }else if (txtAddress.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.address_required), Toast.LENGTH_SHORT).show();
        }else if (txtPassword.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.password_required), Toast.LENGTH_SHORT).show();
        }else if (txtBirthday.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.birthday_required), Toast.LENGTH_SHORT).show();
        }else if (encodePhoto.equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.id_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", txtName.getText().toString());
                jsonObject.put("email", txtEmail.getText().toString());
                jsonObject.put("mobile", txtHp.getText().toString());
                jsonObject.put("password", txtPassword.getText().toString());
                jsonObject.put("address", txtAddress.getText().toString());
                jsonObject.put("birthday", convertDate(txtBirthday.getText().toString(), "dd MMM yyyy", "yyyy-MM-dd HH:mm:ss"));
                jsonObject.put("id_ext", photoExt);
                jsonObject.put("id_file", getString(R.string.base64_info) + encodePhoto);
                jsonObject.put("avatar", "");
                jsonObject.put("role", role);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"user/create")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            User resp = gson.fromJson(response.toString(), User.class);
                            if (resp.getMessage().equals("success")){
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                                intentToLogin();
                            }else{
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }


    }

    private void insertBuyer(){
        if (txtName.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.name_required), Toast.LENGTH_SHORT).show();
        }else if (txtEmail.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.email_required), Toast.LENGTH_SHORT).show();
        }else if (txtHp.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.mobile_required), Toast.LENGTH_SHORT).show();
        }else if (txtAddress.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.address_required), Toast.LENGTH_SHORT).show();
        }else if (txtPassword.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.password_required), Toast.LENGTH_SHORT).show();
        }else if (txtBirthday.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.birthday_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", txtName.getText().toString());
                jsonObject.put("email", txtEmail.getText().toString());
                jsonObject.put("mobile", txtHp.getText().toString());
                jsonObject.put("password", txtPassword.getText().toString());
                jsonObject.put("address", txtAddress.getText().toString());
                jsonObject.put("birthday", convertDate(txtBirthday.getText().toString(), "dd MMM yyyy", "yyyy-MM-dd HH:mm:ss"));
                jsonObject.put("id_ext", "");
                jsonObject.put("id_file", "");
                jsonObject.put("avatar", "");
                jsonObject.put("role", role);

                //write(getApplicationContext(), "aaaa.txt",jsonObject.toString(1));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"user/create")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            User resp = gson.fromJson(response.toString(), User.class);
                            if (resp.getMessage().equals("success")){
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                                intentToLogin();
                            }else{
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void intentToLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        intentToLogin();
    }
}

package dev.risti.serbuapps.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.model.User;
import dev.risti.serbuapps.utils.AppPermissions;

import static dev.risti.serbuapps.utils.GlobalHelper.createFolder;
import static dev.risti.serbuapps.utils.GlobalHelper.isLoggedIn;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_EMAIL;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_PASSWORD;
import static dev.risti.serbuapps.utils.GlobalVars.userHash;

public class LoginActivity extends AppCompatActivity {

    private static final String[] ALL_PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.RECEIVE_BOOT_COMPLETED
    };

    private static final int WRITE_EXTERNAL_STORAGE_CODE = 901;
    private static final int READ_EXTERNAL_STORAGE_CODE = 902;
    private static final int CAMERA_CODE = 904;
    private static final int ACCESS_FINE_LOCATION_CODE = 905;
    private static final int RECEIVE_BOOT_COMPLETED = 906;
    private static final int ALL_REQUEST_CODE = 999;
    private AppPermissions mRuntimePermission;

    private EditText txtEmail;
    private EditText txtPassword;
    private Button btnLogin;
    private TextView tvForgotPassord;
    private TextView tvRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mRuntimePermission = new AppPermissions(this);

        if (mRuntimePermission.hasPermission(ALL_PERMISSIONS)) {
            createFolder();

            if (isLoggedIn() == true){
                intentToMain();
            }

        }else{
            mRuntimePermission.requestPermission(this, ALL_PERMISSIONS, ALL_REQUEST_CODE);
        }

        txtEmail = findViewById(R.id.txtEmail);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvForgotPassord = findViewById(R.id.tvForgotPassord);
        tvRegister = findViewById(R.id.tvRegister);

        tvForgotPassord.setVisibility(View.GONE);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //rule jika email belum diisi
                if (txtEmail.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), getString(R.string.email_required), Toast.LENGTH_SHORT).show();
                }
                //rule jika password belum diisi
                else if (txtPassword.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), getString(R.string.password_required), Toast.LENGTH_SHORT).show();
                }
                //panggil prosedure login
                else{
                    login();
                }
            }
        });

        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToRegister();
            }
        });
    }

    //prosedur login
    private void login(){

        //panggil progress dialog
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.fetch_data_from_server));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();

        //menghapus data user di db lokal
        userHash.flush();

        //proses login ke api
        //parameter email dan password
        AndroidNetworking.post(BASE_IP + "user/login")
                .addBodyParameter(KEY_EMAIL, txtEmail.getText().toString())
                .addBodyParameter(KEY_PASSWORD, txtPassword.getText().toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //do anything with response

                        Gson gson = new Gson();
                        User user = gson.fromJson(String.valueOf(response), User.class);

                        progress.dismiss();

                        //jika respon id dan emailnya tidak null
                        if (user.getId() != null && user.getEmail() != null && user.getEmail() != null){
                            //Toast.makeText(getApplicationContext(),String.valueOf(user.getMessage()), Toast.LENGTH_SHORT).show();

                            //menyimpan user dari respon ke db lokal
                            //untuk digunakan saat login secara offline
                            //atau tidak perlu login lagi sampai user logout
                            userHash.put(user.getId(), user);

                            //menuju ke menu utama
                            intentToMain();

                        }else{
                            Toast.makeText(getApplicationContext(), getString(R.string.wrong_username_password), Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(),String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case ALL_REQUEST_CODE:
                List<Integer> permissionResults = new ArrayList<>();
                for (int grantResult : grantResults) {
                    permissionResults.add(grantResult);
                }
                if (permissionResults.contains(PackageManager.PERMISSION_DENIED)) {
                    Toast.makeText(this, getString(R.string.all_permission_not_granted), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(this, getString(R.string.all_permission_granted), Toast.LENGTH_SHORT).show();
                }
                break;
            case WRITE_EXTERNAL_STORAGE_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, getString(R.string.write_external_permission_not_granted), Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(this, "Write External Storage Permissions granted", Toast.LENGTH_SHORT).show();
                }
                finish();
                break;
            case READ_EXTERNAL_STORAGE_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, getString(R.string.read_external_permission_not_granted), Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(this, "Read External Storage Permissions granted", Toast.LENGTH_SHORT).show();
                }
                finish();
                break;
            case CAMERA_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, getString(R.string.camera_permission_not_granted), Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(this, "Camera Permissions granted", Toast.LENGTH_SHORT).show();
                }
                finish();
                break;
            case ACCESS_FINE_LOCATION_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    Toast.makeText(this, getString(R.string.location_permission_not_granted), Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(this, "Access Location Permissions granted", Toast.LENGTH_SHORT).show();
                }
                finish();
                break;

        }
    }

    private void intentToRegister(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    //prosedur pindah halaman
    //ke halaman utama
    private void intentToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}

package dev.risti.serbuapps.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.model.Category;
import dev.risti.serbuapps.model.Product;
import dev.risti.serbuapps.model.User;

import static dev.risti.serbuapps.utils.GlobalHelper.convertDate;
import static dev.risti.serbuapps.utils.GlobalHelper.getFileNameFromURL;
import static dev.risti.serbuapps.utils.GlobalHelper.getPriceInIdr;
import static dev.risti.serbuapps.utils.GlobalHelper.getRole;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserId;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_ID;

public class ProductDetailActivity extends AppCompatActivity {

    private ImageView ivIncrease;
    private ImageView ivDecrease;
    private ImageView ivImage;
    private TextView tvSellerName;
    private TextView tvProductName;
    private TextView tvAddCart;
    private TextView tvCounter;
    private TextView tvStock;
    private TextView tvPrice;
    private EditText txtDesc;
    private int number = 0;
    private RatingBar ratingbar;

    private Gson gson;
    private String id;
    private String from = "";

    private float rate;
    private int stock;
    private int price;
    private String sellerId;
    private String productId;
    private LinearLayout seller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        gson = new Gson();

        ivIncrease = findViewById(R.id.ivIncrease);
        ivDecrease = findViewById(R.id.ivDecrease);
        tvCounter = findViewById(R.id.tvCounter);
        ratingbar = findViewById(R.id.ratingbar);
        ivImage = findViewById(R.id.ivImage);
        tvSellerName = findViewById(R.id.tvSellerName);
        tvProductName = findViewById(R.id.tvProductName);
        tvAddCart = findViewById(R.id.tvAddCart);
        tvStock = findViewById(R.id.tvStock);
        tvPrice = findViewById(R.id.tvPrice);
        txtDesc = findViewById(R.id.txtDesc);
        seller = findViewById(R.id.seller);

        tvCounter.setText(String.valueOf(number));

        ivDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (number>0){
                    number -= 1;
                    tvCounter.setText(String.valueOf(number));
                }


            }
        });
        ivIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (number<stock){
                    number += 1;
                    tvCounter.setText(String.valueOf(number));
                }
            }
        });

        tvAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getRole().equals("seller")){
                    Toast.makeText(getApplicationContext(), getString(R.string.action_denied), Toast.LENGTH_SHORT).show();
                }else{
                    addCart();
                }
            }
        });

        seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intentToProfile(sellerId);
            }
        });

        LayerDrawable stars = (LayerDrawable) ratingbar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(R.color.yellow), PorterDuff.Mode.SRC_ATOP);

        id = getIntent().getStringExtra(KEY_ID);

        if (getIntent().hasExtra("from")) {
            from = getIntent().getStringExtra("from");
        }


        getData(id);
    }

    private void getData(String id) {

        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.fetch_data_from_server));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();

        AndroidNetworking.post(BASE_IP+"product/readById")
                .addBodyParameter("id", id)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            if (response.getString("message").equals("success")){

                                Product product = gson.fromJson(response.toString(), Product.class);

                                if (product.getRate() != null && !product.getRate().equals(""))
                                    rate = Float.parseFloat(product.getRate());
                                else
                                    rate = 0f;

                                if (product.getQty() != null && !product.getQty().equals(""))
                                    stock = Integer.parseInt(product.getQty());
                                else
                                    stock = 0;

                                price = Integer.parseInt(product.getPrice());

                                ratingbar.setRating(rate);
                                tvSellerName.setText(product.getSeller_name());
                                tvProductName.setText(product.getName());
                                tvStock.setText(String.valueOf(stock));
                                tvPrice.setText(getPriceInIdr(String.valueOf(price)));

                                sellerId = product.getSeller_id();
                                productId = product.getId();

                                //Log.e("sellerId", sellerId);

                                Glide.with(getApplicationContext())
                                        .load(product.getImage())
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .centerCrop()
                                        .error(R.drawable.ic_preview)
                                        .into(ivImage);
                            }else{
                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progress.dismiss();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void addCart(){

        if (number<1){
            Toast.makeText(getApplicationContext(), getString(R.string.number_order_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject mainObj = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject child = new JSONObject();
            try {
                child.put("transaction_id", "0");
                child.put("seller_id", sellerId);
                child.put("buyer_id", getUserId());
                child.put("product_id", productId);
                child.put("qty", String.valueOf(number));
                child.put("price", price);
                child.put("note", txtDesc.getText().toString());
                child.put("rate", "0");
                child.put("status", "booked");

                jsonArray.put(child);
                mainObj.put("item", jsonArray);

                //write(getApplicationContext(), "aaaa.txt",jsonObject.toString(1));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"transaction_detail/create_cart")
                    .addJSONObjectBody(mainObj) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            // do anything with response
                            try {
                                User resp = gson.fromJson(response.getJSONObject(0).toString(), User.class);
                                if (resp.getMessage().equals("success")){
                                    progress.dismiss();
                                    Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                                    intentToCart();
                                }else{
                                    progress.dismiss();
                                    Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void intentToList(){
        Intent intent = new Intent(this, ProductListActivity.class);
        startActivity(intent);
        finish();
    }

    private void intentToCategory(){
        Intent intent = new Intent(this, CategoryListActivity.class);
        startActivity(intent);
        finish();
    }


    private void intentToProfile(String userId){
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("from", "product");
        intent.putExtra("userId", userId);
        startActivity(intent);
        finish();
    }

    private void intentToCart(){
        Intent intent = new Intent(this, CartListActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();

        if (from!=null && from.equals("section"))
            intentToCategory();
        else
            intentToList();
    }
}

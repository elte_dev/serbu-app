package dev.risti.serbuapps.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.adapter.ProductAdapter;
import dev.risti.serbuapps.interfaces.PaginationAdapterCallback;
import dev.risti.serbuapps.interfaces.PaginationScrollListenerGrid;
import dev.risti.serbuapps.interfaces.RecyclerviewClick;
import dev.risti.serbuapps.model.Product;
import dev.risti.serbuapps.utils.SharedPref;

import static dev.risti.serbuapps.utils.GlobalHelper.getRole;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserId;
import static dev.risti.serbuapps.utils.GlobalHelper.setBadgeCount;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_CART_COUNTER;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_CATEGORY_ID;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_ID;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_OWN_PRODUCT;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_TRUE;

public class ProductListActivity extends AppCompatActivity implements PaginationAdapterCallback {

    private ProductAdapter adapter;

    private Gson gson;

    //LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;

    private RecyclerView rv;
    private ProgressBar progressBar;
    private LinearLayout errorLayout;
    private Button btnRetry;
    private TextView txtError;
    private LinearLayout parentLayout;
    //private MaterialSearchView searchView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView tvNoData;

    private static final int PAGE_START = 1;

    private boolean isLoading = false;
    private boolean isLastPage = false;

    private int TOTAL_PAGES = 1;
    private int currentPage = PAGE_START;

    private String id = "";
    private String ownProduct = "";

    private int mNotificationsCount = 0;
    private SharedPref sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        id = getIntent().getStringExtra(KEY_CATEGORY_ID);
        ownProduct = getIntent().getStringExtra(KEY_OWN_PRODUCT);
        gson = new Gson();
        sharedPref = new SharedPref(this);
        if (sharedPref.getString(KEY_CART_COUNTER)!=null && !sharedPref.getString(KEY_CART_COUNTER).equals(""))
            mNotificationsCount = Integer.parseInt(sharedPref.getString(KEY_CART_COUNTER));
        else
            mNotificationsCount = 0;

        //Log.e("ownProduct", ownProduct);

        rv = findViewById(R.id.rv);
        progressBar = findViewById(R.id.main_progress);
        errorLayout = findViewById(R.id.error_layout);
        btnRetry = findViewById(R.id.error_btn_retry);
        txtError = findViewById(R.id.error_txt_cause);
        parentLayout = findViewById(R.id.parentLayout);
        //searchView = findViewById(R.id.search_view);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        tvNoData = findViewById(R.id.tvNoData);

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        adapter = new ProductAdapter(this, new RecyclerviewClick() {
            @Override
            public void onItemClick(View v, int position) {

            }
        });

        //linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);

        rv.setLayoutManager(gridLayoutManager);

        //rv.setLayoutManager(linearLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadFirstPage(id);
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                loadFirstPage(id);
            }
        });

//        rv.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
//            @Override
//            public void onLoadMore(int page, int totalItemsCount) {
//                isLoading = true;
//                currentPage = currentPage + 1;
//
//                loadNextPage(id);
//            }
//        });

        rv.addOnScrollListener(new PaginationScrollListenerGrid(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage = currentPage + 1;

                loadNextPage(id);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadFirstPage(id);
            }
        });
    }

    private void loadFirstPage(String key) {

        if (progressBar!=null && progressBar.getVisibility()==View.GONE)
            progressBar.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
        hideErrorView();

        if (adapter!=null && !adapter.isEmpty())
            adapter.clearAll();

        currentPage = 1;
        isLastPage = false;

        String url = "";

        if (getRole().equals("buyer") && id!=null && !id.equals("")){
            url = BASE_IP+"product/readByCategoryId?category_id="+ key + "&page=";
        }else if (ownProduct!=null && !ownProduct.equals("") && ownProduct.equals(KEY_TRUE)){
            url = BASE_IP+"product/readBySeller?key="+ getUserId() + "&page=";
        } else{
            url = BASE_IP+"product/readAll?page=";
        }

        AndroidNetworking.get(url+PAGE_START)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideErrorView();
                        List<Product> results = new ArrayList<>();
                        try {
                            String records = response.getString("records");
                            String paging = response.getString("paging");
                            JSONObject pageObj = new JSONObject(paging);

                            TOTAL_PAGES = pageObj.getInt("total_pages");

                            JSONArray dataArr = new JSONArray(records);

                            if (dataArr.length()>0){
                                for (int i = 0; i < dataArr.length(); i++) {
                                    Product product = gson.fromJson(dataArr.getJSONObject(i).toString(), Product.class);
                                    results.add(product);
                                }
                            }

                            if (results.isEmpty()){
                                tvNoData.setVisibility(View.VISIBLE);
                                rv.setVisibility(View.GONE);
                            }else{
                                tvNoData.setVisibility(View.GONE);
                                rv.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //List<Po> results = fetchResults(response);
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.addAll(results);


                        if (currentPage <= TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        } else {
                            isLastPage = true;
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        showErrorView(error);
                    }
                });
    }

    private void loadNextPage(String key) {
        hideErrorView();

        String url = "";
        if (getRole().equals("buyer") && id!=null && !id.equals("")){
            url = BASE_IP+"product/readByCategoryId?category_id=" + key +"&page=";
        }else if (ownProduct!=null && !ownProduct.equals("") && ownProduct.equals(KEY_TRUE)){
            url = BASE_IP+"product/readBySeller?key="+  getUserId() + "&page=";
        }else{
            url = BASE_IP+"product/readAll?page=";
        }


        AndroidNetworking.get(url+currentPage)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        // do anything with response
                        hideErrorView();

                        List<Product> results = new ArrayList<>();
                        try {
                            String message = response.getString("message");
                            String records = response.getString("records");
                            String paging = response.getString("paging");
                            JSONObject pageObj = new JSONObject(paging);

                            TOTAL_PAGES = pageObj.getInt("total_pages");
                            JSONArray dataArr = new JSONArray(records);

                            if (dataArr.length()>0){
                                for (int i = 0; i < dataArr.length(); i++) {
                                    Product product = gson.fromJson(dataArr.getJSONObject(i).toString(), Product.class);
                                    results.add(product);
                                }
                            }

                            if (results.isEmpty()){
                                tvNoData.setVisibility(View.VISIBLE);
                                rv.setVisibility(View.GONE);
                            }else{
                                tvNoData.setVisibility(View.GONE);
                                rv.setVisibility(View.VISIBLE);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter.removeLoadingFooter();
                        isLoading = false;

                        adapter.addAll(results);

                        if (currentPage < TOTAL_PAGES) {
                            adapter.addLoadingFooter();
                        }
                        else {
                            isLastPage = true;
                        }


                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        showErrorView(error);
                    }
                });
    }

    private void searchByKey(String key, String category_id) {
        hideErrorView();
        progressBar.setVisibility(View.VISIBLE);
        AndroidNetworking.post(BASE_IP+"product/searchByKey")
                .addBodyParameter("key", key)
                .addBodyParameter("category_id", category_id)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideErrorView();
                        List<Product> results = new ArrayList<>();
                        try {
                            String records = response.getString("records");

                            JSONArray dataArr = new JSONArray(records);

                            if (dataArr.length()>0){
                                for (int i = 0; i < dataArr.length(); i++) {
                                    Product product = gson.fromJson(dataArr.getJSONObject(i).toString(), Product.class);
                                    results.add(product);
                                }
                            }

                            if (results.isEmpty()){
                                tvNoData.setVisibility(View.VISIBLE);
                                rv.setVisibility(View.GONE);
                            }else{
                                tvNoData.setVisibility(View.GONE);
                                rv.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            tvNoData.setVisibility(View.VISIBLE);
                            rv.setVisibility(View.GONE);
                        }

                        //List<Po> results = fetchResults(response);
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.addAll(results);

                        isLastPage = true;
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        showErrorView(error);
                    }
                });
    }

    private void searchByName(String key) {
        hideErrorView();
        progressBar.setVisibility(View.VISIBLE);
        AndroidNetworking.post(BASE_IP+"product/searchByName")
                .addBodyParameter("key", key)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideErrorView();
                        List<Product> results = new ArrayList<>();
                        try {
                            String records = response.getString("records");

                            JSONArray dataArr = new JSONArray(records);

                            if (dataArr.length()>0){
                                for (int i = 0; i < dataArr.length(); i++) {
                                    Product product = gson.fromJson(dataArr.getJSONObject(i).toString(), Product.class);
                                    results.add(product);
                                }
                            }

                            if (results.isEmpty()){
                                tvNoData.setVisibility(View.VISIBLE);
                                rv.setVisibility(View.GONE);
                            }else{
                                tvNoData.setVisibility(View.GONE);
                                rv.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            tvNoData.setVisibility(View.VISIBLE);
                            rv.setVisibility(View.GONE);
                        }

                        //List<Po> results = fetchResults(response);
                        progressBar.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                        adapter.addAll(results);

                        isLastPage = true;
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        showErrorView(error);
                    }
                });
    }

    private void showErrorView(Throwable throwable) {

        if (errorLayout.getVisibility() == View.GONE) {
            errorLayout.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);

            txtError.setText(fetchErrorMessage(throwable));
        }
    }

    /**
     * @param throwable to identify the type of error
     * @return appropriate error message
     */
    private String fetchErrorMessage(Throwable throwable) {
        String errorMsg = getResources().getString(R.string.error_msg_unknown);

        if (!isNetworkConnected()) {
            errorMsg = getResources().getString(R.string.error_msg_no_internet);
        } else if (throwable instanceof TimeoutException) {
            errorMsg = getResources().getString(R.string.error_msg_timeout);
        }

        return errorMsg;
    }

    // Helpers -------------------------------------------------------------------------------------

    private void hideErrorView() {
        if (errorLayout.getVisibility() == View.VISIBLE) {
            errorLayout.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Remember to add android.permission.ACCESS_NETWORK_STATE permission.
     *
     * @return
     */
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void retryPageLoad() {
        loadNextPage(id);
    }

    private void intentToCategory(){
        Intent intent = new Intent(this, CategoryListActivity.class);
        startActivity(intent);
        finish();
    }

    public void intentToDetail(String id){
        Intent intent = new Intent(this, ProductDetailActivity.class);
        intent.putExtra(KEY_ID, id);
        startActivity(intent);
        finish();
    }

    public void intentToUpdate(String id){
        Intent intent = new Intent(this, UpdateProductActivity.class);
        intent.putExtra(KEY_ID, id);
        startActivity(intent);
        finish();
    }

    private void intentToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem item = menu.findItem(R.id.menu_item_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        setBadgeCount(this, icon, mNotificationsCount);

        MenuItem searchViewItem = menu.findItem(R.id.menu_item_search);
        final SearchView searchViewAndroidActionBar = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        searchViewAndroidActionBar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchViewAndroidActionBar.clearFocus();

                if (getRole().equals("buyer") && id!=null && !id.equals("")){
                    searchByKey(query,id);
                }else{
                    searchByName(query);
                }


                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (adapter!=null && !adapter.isEmpty())
                    adapter.clearAll();
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void updateNotificationsBadge(int count) {
        mNotificationsCount = count;

        // force the ActionBar to relayout its MenuItems.
        // onCreateOptionsMenu(Menu) will be called again.
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();
        if (getRole().equals("buyer") && id!=null && !id.equals(""))
            intentToCategory();
        else
            intentToMain();
    }
}

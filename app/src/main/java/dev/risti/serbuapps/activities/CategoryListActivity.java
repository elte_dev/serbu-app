package dev.risti.serbuapps.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.adapter.RecyclerViewDataAdapter;
import dev.risti.serbuapps.model.Category;
import dev.risti.serbuapps.utils.SharedPref;

import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.KEY_CART_COUNTER;

public class CategoryListActivity extends AppCompatActivity {

    private List<Category> list;
    private JSONArray arr;
    private RecyclerView rv;
    private RecyclerViewDataAdapter adapter;
    private Gson gson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        rv = findViewById(R.id.rv);
        gson = new Gson();
        getData();
    }

//    private void getData(){
//        list = new ArrayList<Category>();
//        arr = new JSONArray();
//
//        rv.setHasFixedSize(true);
//
//        RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(CategoryListActivity.this, list);
//
//        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//
//        rv.setAdapter(adapter);
//    }

    private void getData(){
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.fetch_data_from_server));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        AndroidNetworking.get(BASE_IP + "category/readSection")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        try {
                            if (response.getString("message").equals("success")){
                                String data = response.getString("records");

                                list = new ArrayList<Category>();
                                arr = new JSONArray();

                                arr = new JSONArray(data);

                                if (arr.length()>0) {
                                    for (int i = 0; i < arr.length(); i++) {
                                        Category category = gson.fromJson(arr.getJSONObject(i).toString(), Category.class);
                                        list.add(category);
                                    }

                                    rv.setHasFixedSize(true);
                                    adapter = new RecyclerViewDataAdapter(CategoryListActivity.this, list);
                                    rv.setLayoutManager(new LinearLayoutManager(CategoryListActivity.this, LinearLayoutManager.VERTICAL, false));
                                    rv.setAdapter(adapter);
                                }

                            }else{
                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                            //Log.e("respon", response.toString(1));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            try {
                                Toast.makeText(getApplicationContext(),response.getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                        }

                        progress.dismiss();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(getApplicationContext(),String.valueOf(error), Toast.LENGTH_SHORT).show();
                        progress.dismiss();
                    }
                });
    }

//    public void createDummyData() {
//        for (int i = 1; i <= 5; i++) {
//
//            Category dm = new Category();
//
//            dm.setHeaderTitle("Section " + i);
//
//            ArrayList<Product> singleItem = new ArrayList<Product>();
//            for (int j = 0; j <= 5; j++) {
//                singleItem.add(new Product("Item " + j, "URL " + j));
//            }
//
//            dm.setAllItemsInSection(singleItem);
//
//            allSampleData.add(dm);
//
//        }
//    }

    private void intentToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();
        intentToMain();
    }
}

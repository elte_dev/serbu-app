package dev.risti.serbuapps.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import dev.risti.serbuapps.R;
import dev.risti.serbuapps.imagepicker.RxImageConverters;
import dev.risti.serbuapps.imagepicker.RxImagePicker;
import dev.risti.serbuapps.imagepicker.Sources;
import dev.risti.serbuapps.model.User;
import dev.risti.serbuapps.slidedatetimepicker.SlideDateTimeListener;
import dev.risti.serbuapps.slidedatetimepicker.SlideDateTimePicker;
import io.reactivex.Observable;

import static dev.risti.serbuapps.utils.GlobalHelper.compressFoto;
import static dev.risti.serbuapps.utils.GlobalHelper.convertDate;
import static dev.risti.serbuapps.utils.GlobalHelper.convertFileToContentUri;
import static dev.risti.serbuapps.utils.GlobalHelper.convertStringToDateFormat;
import static dev.risti.serbuapps.utils.GlobalHelper.deleteRecursive;
import static dev.risti.serbuapps.utils.GlobalHelper.encodeFileBase64;
import static dev.risti.serbuapps.utils.GlobalHelper.getFileNameFromURL;
import static dev.risti.serbuapps.utils.GlobalHelper.getMimeTypeFromUri;
import static dev.risti.serbuapps.utils.GlobalHelper.getPath;
import static dev.risti.serbuapps.utils.GlobalHelper.getRole;
import static dev.risti.serbuapps.utils.GlobalHelper.getUserId;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_DIR;
import static dev.risti.serbuapps.utils.GlobalVars.BASE_IP;
import static dev.risti.serbuapps.utils.GlobalVars.EXTERNAL_DIR_FILES;
import static dev.risti.serbuapps.utils.GlobalVars.IMAGES_DIR;

public class ProfileActivity extends AppCompatActivity {

    //open camera
    //open camera
    private RadioGroup converterRadioGroup;
    private Uri photoUri = null;
    private Uri finalPhotoUri = null;
    private File compressedImage = null;
    private File tempFile= null;
    private String photoExt = "";
    private String encodePhoto = "";
    private Bitmap theBitmap = null;
    private ImageView ivId;
    private ImageView ivCamera;
    private ImageView ivGalery;

    private RatingBar ratingBar;
    private EditText txtCategory;
    private EditText txtName;
    private EditText txtEmail;
    private EditText txtHp;
    private EditText txtAddress;
    private EditText txtBirthday;
    private ImageView ivDate;
    private EditText txtPassword;
    private ImageView ivPassword;
    private LinearLayout layoutPassword;
    private Button btnUpdate;
    private TextView tvCancel;

    private Gson gson;
    private String id;
    private float rate;
    private String userId = "";
    private String from = "";

    private SimpleDateFormat mFormatter = new SimpleDateFormat("dd MMM yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        gson = new Gson();

        ivId = findViewById(R.id.ivId);
        ivCamera = findViewById(R.id.ivCamera);
        ivGalery = findViewById(R.id.ivGalery);
        ratingBar = findViewById(R.id.ratingBar);
        txtCategory = findViewById(R.id.txtCategory);
        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        txtHp = findViewById(R.id.txtHp);
        txtAddress = findViewById(R.id.txtAddress);
        txtBirthday = findViewById(R.id.txtBirthday);
        ivDate = findViewById(R.id.ivDate);
        txtPassword = findViewById(R.id.txtPassword);
        ivPassword = findViewById(R.id.ivPassword);
        btnUpdate = findViewById(R.id.btnUpdate);
        tvCancel = findViewById(R.id.tvCancel);
        layoutPassword = findViewById(R.id.layoutPassword);

        from = getIntent().getStringExtra("from");

        if (getIntent().getStringExtra("userId") !=null){
            userId = getIntent().getStringExtra("userId");
        }else{
            userId = getUserId();
        }


        if (getRole().equals("buyer")){

            if (from!=null && !from.equals("")){
                ratingBar.setVisibility(View.VISIBLE);
                tvCancel.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.GONE);
                layoutPassword.setVisibility(View.GONE);
                ivDate.setVisibility(View.GONE);
                ivCamera.setVisibility(View.GONE);
                ivGalery.setVisibility(View.GONE);
            } else{
                ratingBar.setVisibility(View.GONE);
            }

        }else{
            ratingBar.setVisibility(View.VISIBLE);
            if (!userId.equals(getUserId())){
                tvCancel.setVisibility(View.GONE);
                btnUpdate.setVisibility(View.GONE);
                layoutPassword.setVisibility(View.GONE);
                ivDate.setVisibility(View.GONE);
                ivCamera.setVisibility(View.GONE);
                ivGalery.setVisibility(View.GONE);
            }
        }



        //open camera
        converterRadioGroup = findViewById(R.id.radio_group);
        converterRadioGroup.check(R.id.radio_file);

        if (RxImagePicker.with(ProfileActivity.this).getActiveSubscription() != null) {
            RxImagePicker.with(ProfileActivity.this).getActiveSubscription().subscribe(this::onImagePicked);
        }

        ivCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromSource(Sources.CAMERA);
            }
        });

        ivGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickImageFromSource(Sources.GALLERY);
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentToMain();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getRole().equals("buyer"))
                    updateBuyer();
                else
                    updateSeller();
            }
        });

        ivDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String newDate = convertDate(txtBirthday.getText().toString(), "dd MMM yyyy", "EEE MMM HH:mm:ss z yyyy");
                Date date = convertStringToDateFormat(newDate, "EEE MMM HH:mm:ss z yyyy");

                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                    .setListener(listener)
                    .setInitialDate(date)
                    //.setMinDate(minDate)
                    // .setMaxDate(maxDate)
                    .setIs24HourTime(true)
                    //.setTheme(SlideDateTimePicker.HOLO_DARK)
                    // .setIndicatorColor(Color.parseColor("#990000"))
                    .build()
                    .show();
            }
        });

        layoutPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomDialog();
            }
        });

        getData();
    }

    private void pickImageFromSource(Sources source) {

        RxImagePicker.with(ProfileActivity.this).requestImage(source)
                .flatMap(uri -> {
                    switch (converterRadioGroup.getCheckedRadioButtonId()) {
                        case R.id.radio_file:
                            return RxImageConverters.uriToFile(ProfileActivity.this, uri, createTempFile());
                        case R.id.radio_bitmap:
                            return RxImageConverters.uriToBitmap(ProfileActivity.this, uri);
                        default:
                            return Observable.just(uri);
                    }
                })
                .subscribe(this::onImagePicked, throwable -> Toast.makeText(ProfileActivity.this, String.format("Error: %s", throwable), Toast.LENGTH_LONG).show());
    }

    private File createTempFile() {
        return new File(BASE_DIR + EXTERNAL_DIR_FILES, id + ".jpeg");
    }

    private void onImagePicked(Object result) {
        if (result instanceof Bitmap) {
            //ivImage.setImageBitmap((Bitmap) result);
        }else{
            photoUri = Uri.parse(String.valueOf(result));

            tempFile = new File(String.valueOf(photoUri));

            compressedImage = compressFoto(ProfileActivity.this, tempFile);


            try {
                finalPhotoUri = convertFileToContentUri(ProfileActivity.this, compressedImage);

            } catch (Exception e) {
                e.printStackTrace();
            }

            photoExt = "."+getMimeTypeFromUri(ProfileActivity.this, finalPhotoUri);
            encodePhoto = encodeFileBase64(getPath(ProfileActivity.this, finalPhotoUri));

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    if (Looper.myLooper() == null) {
                        Looper.prepare();
                    }

                    try {
                        theBitmap = Glide.
                                with(ProfileActivity.this).
                                load(getPath(ProfileActivity.this, finalPhotoUri)).
                                asBitmap().
                                into(100,100).
                                get();
                    } catch (final ExecutionException e) {
                        Log.e("TAG","ExecutionException " + e.getMessage());
                    } catch (final InterruptedException e) {
                        Log.e("TAG","InterruptedException " + e.getMessage());
                    }
                    return null;
                }
                @Override
                protected void onPostExecute(Void dummy) {
                    if (null != theBitmap) {
                        // The full bitmap should be available here
                        //ivAvatar.setImageBitmap(theBitmap);

                        File mypath=new File(IMAGES_DIR,id+".jpeg");

                        ContextWrapper cw = new ContextWrapper(ProfileActivity.this);
                        // path to /data/data/yourapp/app_data/imageDir
                        // Create imageDir
                        //File mypath=new File(fotoPath,userId+".jpeg");

                        FileOutputStream fos = null;
                        try {
                            fos = new FileOutputStream(mypath);
                            // Use the compress method on the BitMap object to write image to the OutputStream
                            theBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            try {
                                fos.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        Glide.with(getApplicationContext())
                                .load(mypath)
                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                                .placeholder(R.drawable.ic_preview)
                                .into(ivId);

                        Log.d("TAG", "Image loaded");
                    };
                }
            }.execute();

            deleteRecursive(new File(String.valueOf(finalPhotoUri)));
            deleteRecursive(createTempFile());
            deleteRecursive(tempFile);

        }
    }

    private void getData() {

        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.fetch_data_from_server));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();

        AndroidNetworking.post(BASE_IP+"user/readById")
                .addBodyParameter("id", userId)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            if (response.getString("message").equals("success")){
                                String records = response.getString("records");

                                JSONObject jsonObject = new JSONObject(records);

                                User user = gson.fromJson(jsonObject.toString(), User.class);

                                if (user.getRate() != null && !user.getRate().equals(""))
                                    rate = Float.parseFloat(user.getRate());
                                else
                                    rate = 0f;

                                ratingBar.setRating(rate);
                                txtCategory.setText(user.getRole());
                                txtName.setText(user.getName());
                                txtEmail.setText(user.getEmail());
                                txtHp.setText(user.getMobile());
                                txtAddress.setText(user.getAddress());

                                //String newDate = convertDate(user.getBirthday(), "yyyy-MM-dd", "EEE MMM HH:mm:ss z yyyy");
                                //Date date = convertStringToDateFormat(newDate, "EEE MMM HH:mm:ss z yyyy");

                                id = getFileNameFromURL(user.getAvatar());

//                                new SlideDateTimePicker.Builder(getSupportFragmentManager())
//                                        .setListener(listener)
//                                        .setInitialDate(date)
//                                        //.setMinDate(minDate)
//                                        // .setMaxDate(maxDate)
//                                        .setIs24HourTime(true)
//                                        //.setTheme(SlideDateTimePicker.HOLO_DARK)
//                                        // .setIndicatorColor(Color.parseColor("#990000"))
//                                        .build()
//                                        .show();

                                txtBirthday.setText(convertDate(user.getBirthday(), "yyyy-MM-dd", "dd MMM yyyy"));
                                txtPassword.setText("12345");

                                Glide.with(ProfileActivity.this)
                                        .load(user.getAvatar())
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .centerCrop()
                                        .error(R.drawable.ic_preview)
                                        .into(ivId);
                            }else{
                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progress.dismiss();
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Toast.makeText(getApplicationContext(), String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private SlideDateTimeListener listener = new SlideDateTimeListener() {
        @Override
        public void onDateTimeSet(Date date) {
            txtBirthday.setText(mFormatter.format(date));
        }

        // Optional cancel listener
        @Override
        public void onDateTimeCancel() {
        }
    };

//    private void CustomDialog(){
//        final Dialog dialog = new Dialog(this, android.R.style.Theme_Material_Light_NoActionBar);
//        dialog.setContentView(R.layout.view_change_password);
//        EditText txtPassword =dialog.findViewById(R.id.txtPassword);
//        EditText txtNewPassword =dialog.findViewById(R.id.txtNewPassword);
//        EditText txtNewPasswordAgain =dialog.findViewById(R.id.txtNewPasswordAgain);
//        Button btnUpdate =  dialog.findViewById(R.id.btnUpdate);
//        TextView tvCancel =  dialog.findViewById(R.id.tvCancel);
//
//        dialog.show();
//
//        tvCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        btnUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (txtPassword.getText().toString().equals("")){
//                    Toast.makeText(getApplicationContext(), getString(R.string.password_required), Toast.LENGTH_SHORT).show();
//                }else if (txtNewPassword.getText().toString().equals("")){
//                    Toast.makeText(getApplicationContext(), getString(R.string.new_password_required), Toast.LENGTH_SHORT).show();
//                }else if (txtNewPasswordAgain.getText().toString().equals("")){
//                    Toast.makeText(getApplicationContext(), getString(R.string.new_password_again_required), Toast.LENGTH_SHORT).show();
//                }else if (txtPassword.getText().toString().equals(txtNewPassword.getText().toString())){
//                    Toast.makeText(getApplicationContext(), getString(R.string.new_password_is_same), Toast.LENGTH_SHORT).show();
//                }else if (!txtNewPassword.getText().toString().equals(txtNewPasswordAgain.getText().toString())){
//                    Toast.makeText(getApplicationContext(), getString(R.string.new_password_did_not_match), Toast.LENGTH_SHORT).show();
//                }else{
//                    dialog.dismiss();
//                }
//            }
//        });
//    }

    private void CustomDialog(){
        LayoutInflater li = LayoutInflater.from(ProfileActivity.this);
        View promptsView = li.inflate(R.layout.view_change_password, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ProfileActivity.this);

        alertDialogBuilder
                .setTitle(getString(R.string.change_password))
                .setIcon(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.ic_info_black_24dp));

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        EditText txtPassword =promptsView.findViewById(R.id.txtPassword);
        EditText txtNewPassword =promptsView.findViewById(R.id.txtNewPassword);
        EditText txtNewPasswordAgain =promptsView.findViewById(R.id.txtNewPasswordAgain);
        Button btnUpdate =  promptsView.findViewById(R.id.btnUpdate);
        TextView tvCancel =  promptsView.findViewById(R.id.tvCancel);

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtPassword.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), getString(R.string.password_required), Toast.LENGTH_SHORT).show();
                }else if (txtNewPassword.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), getString(R.string.new_password_required), Toast.LENGTH_SHORT).show();
                }else if (txtNewPasswordAgain.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), getString(R.string.new_password_again_required), Toast.LENGTH_SHORT).show();
                }else if (txtPassword.getText().toString().equals(txtNewPassword.getText().toString())){
                    Toast.makeText(getApplicationContext(), getString(R.string.new_password_is_same), Toast.LENGTH_SHORT).show();
                }else if (!txtNewPassword.getText().toString().equals(txtNewPasswordAgain.getText().toString())){
                    Toast.makeText(getApplicationContext(), getString(R.string.new_password_did_not_match), Toast.LENGTH_SHORT).show();
                }else{
                    alertDialog.cancel();
                    updatePassword(txtNewPasswordAgain.getText().toString());
                }
            }
        });

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.cancel();
            }
        });
    }

    private void updateSeller(){

        if (txtName.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.name_required), Toast.LENGTH_SHORT).show();
        }else if (txtEmail.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.email_required), Toast.LENGTH_SHORT).show();
        }else if (txtHp.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.mobile_required), Toast.LENGTH_SHORT).show();
        }else if (txtAddress.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.address_required), Toast.LENGTH_SHORT).show();
        }else if (txtPassword.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.password_required), Toast.LENGTH_SHORT).show();
        }else if (txtBirthday.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.birthday_required), Toast.LENGTH_SHORT).show();
        }else if (encodePhoto.equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.id_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", userId);
                jsonObject.put("name", txtName.getText().toString());
                jsonObject.put("email", txtEmail.getText().toString());
                jsonObject.put("mobile", txtHp.getText().toString());
                jsonObject.put("address", txtAddress.getText().toString());
                jsonObject.put("birthday", convertDate(txtBirthday.getText().toString(), "dd MMM yyyy", "yyyy-MM-dd HH:mm:ss"));
                jsonObject.put("id_ext", photoExt);
                jsonObject.put("id_file", getString(R.string.base64_info) + encodePhoto);
                jsonObject.put("avatar", userId+photoExt);
                jsonObject.put("avatar_file", getString(R.string.base64_info) + encodePhoto);
                jsonObject.put("role", txtCategory.getText().toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"user/update")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            User resp = gson.fromJson(response.toString(), User.class);
                            if (resp.getMessage().equals("success")){
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                                intentToMain();
                            }else{
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }


    }

    private void updateBuyer(){
        if (txtName.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.name_required), Toast.LENGTH_SHORT).show();
        }else if (txtEmail.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.email_required), Toast.LENGTH_SHORT).show();
        }else if (txtHp.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.mobile_required), Toast.LENGTH_SHORT).show();
        }else if (txtAddress.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.address_required), Toast.LENGTH_SHORT).show();
        }else if (txtPassword.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.password_required), Toast.LENGTH_SHORT).show();
        }else if (txtBirthday.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(), getString(R.string.birthday_required), Toast.LENGTH_SHORT).show();
        }else{
            final ProgressDialog progress = new ProgressDialog(this);
            progress.setMessage(getString(R.string.information));
            progress.setTitle(getString(R.string.please_wait));
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.show();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("id", userId);
                jsonObject.put("name", txtName.getText().toString());
                jsonObject.put("email", txtEmail.getText().toString());
                jsonObject.put("mobile", txtHp.getText().toString());
                jsonObject.put("address", txtAddress.getText().toString());
                jsonObject.put("birthday", convertDate(txtBirthday.getText().toString(), "dd MMM yyyy", "yyyy-MM-dd HH:mm:ss"));
                jsonObject.put("id_ext", "");
                jsonObject.put("id_file", "");
                jsonObject.put("avatar", userId+photoExt);
                jsonObject.put("avatar_file", getString(R.string.base64_info) + encodePhoto);
                jsonObject.put("role", txtCategory.getText().toString());

                //write(getApplicationContext(), "aaaa.txt",jsonObject.toString(1));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            AndroidNetworking.post(BASE_IP+"user/update")
                    .addJSONObjectBody(jsonObject) // posting json
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            User resp = gson.fromJson(response.toString(), User.class);
                            if (resp.getMessage().equals("success")){
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                                intentToMain();
                            }else{
                                progress.dismiss();
                                Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void updatePassword(String password){
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.information));
        progress.setTitle(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", userId);
            jsonObject.put("password", password);

            //write(getApplicationContext(), "aaaa.txt",jsonObject.toString(1));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(BASE_IP+"user/updatePassword")
                .addJSONObjectBody(jsonObject) // posting json
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        User resp = gson.fromJson(response.toString(), User.class);
                        if (resp.getMessage().equals("success")){
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                            intentToMain();
                        }else{
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(),resp.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progress.dismiss();
                        Toast.makeText(getApplicationContext(),"error " + String.valueOf(error), Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void intentToMain(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed(){
        intentToMain();
    }
}
